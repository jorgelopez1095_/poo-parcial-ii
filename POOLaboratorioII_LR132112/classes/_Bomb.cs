﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POOLaboratorioII_LR132112.classes
{
    class _Bomb
    {
        private double special_gasoline = 600.00;
        private double regular_gasoline = 800.00;
        private double diesel_gasoline  = 600.00;

        private double special_price = 4.45;
        private double regular_price = 3.95;
        private double diesel_price  = 4.25;

        private double total_vendido;

        public double Special_gasoline { get => special_gasoline;  set { if (value >= 0) { special_gasoline    = value;   } } }
        public double Regular_gasoline { get => regular_gasoline;  set { if (value >= 0) { regular_gasoline    = value;   } } }
        public double Diesel_gasoline  { get => diesel_gasoline;   set { if (value >= 0) { diesel_gasoline     = value;   } } }

        public double Special_price { get => special_price; set => special_price    = value; }
        public double Regular_price { get => regular_price; set => regular_price    = value; }
        public double Diesel_price  { get => diesel_price;  set => diesel_price     = value; }
        public double Total_vendido { get => total_vendido; set => total_vendido = value; }
    }
}
