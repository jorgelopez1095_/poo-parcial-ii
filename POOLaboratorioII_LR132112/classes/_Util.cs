﻿using MaterialSkin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POOLaboratorioII_LR132112.classes
{

    /*
     * This class contains utilities of application.
     */

    class _Util
    {
        List<_Bomb> bombs = new List<_Bomb>();

        public _Util() {}

        MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;

        public void setMaterialSkin(FrmSplashScreen context)
        {
            materialSkinManager.AddFormToManage(context);
            setPropsMaterialSkin();
        }


        public void setMaterialSkin(FrmMainView context)
        {
            materialSkinManager.AddFormToManage(context);
            setPropsMaterialSkin();
        }


        public void setPropsMaterialSkin()
        {
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(
                Primary.Green600,
                Primary.BlueGrey900,
                Primary.BlueGrey500,
                Accent.LightBlue200, TextShade.WHITE);
        }

        public List<_Bomb> bombsList()
        {
            for(int i = 1; i <= 6; i++)
            {
                _Bomb bomb = new _Bomb();
                bomb.Total_vendido = 0; //Inicialmente comienza con un monto de 0.
                bombs.Add(bomb);
            }

            return bombs;
        }

        public double discoutnWithMoney(int bomb, double bombTotal, double gasolinePrice, double moneyValue)
        {
            double discount = moneyValue / gasolinePrice;
            return Math.Truncate(discount * 100) / 100;
        }

        public double discountWithGallons(int bomb, double bombTotal, double gasolinePrice, int quantityGallons)
        {
            /* Si la cantidad que desea bombear excede al total de lo de la bomba, hace nada retornando un 0 */
            if(quantityGallons > bombTotal)
            {
                return 0;
            } else
            {
                return quantityGallons;
            }

            return 0;
        }

       

    }

}
