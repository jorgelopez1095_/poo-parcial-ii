﻿namespace POOLaboratorioII_LR132112
{
    partial class FrmMainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMainView));
            this.tab_selector_bombs = new MaterialSkin.Controls.MaterialTabSelector();
            this.tab_container_bombs = new MaterialSkin.Controls.MaterialTabControl();
            this.tab_page_bombs_1_3 = new System.Windows.Forms.TabPage();
            this.gbx_bomb_3 = new System.Windows.Forms.GroupBox();
            this.tab_selector_bomb_3 = new MaterialSkin.Controls.MaterialTabSelector();
            this.tab_control_bomb_3 = new MaterialSkin.Controls.MaterialTabControl();
            this.tab_page_money_3 = new System.Windows.Forms.TabPage();
            this.lbl_gasoline_type_3 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_3 = new System.Windows.Forms.ComboBox();
            this.btn_bomb_money_3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_gallons_3 = new System.Windows.Forms.TabPage();
            this.lbl_gasoline_type_3_1 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_3_1 = new System.Windows.Forms.ComboBox();
            this.btn_bombear_galones_3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_info_3 = new System.Windows.Forms.TabPage();
            this.pbx_bomb_3 = new System.Windows.Forms.PictureBox();
            this.gbx_bomb_2 = new System.Windows.Forms.GroupBox();
            this.tab_selector_bomb_2 = new MaterialSkin.Controls.MaterialTabSelector();
            this.tab_control_bomb_2 = new MaterialSkin.Controls.MaterialTabControl();
            this.tab_page_bomb_2 = new System.Windows.Forms.TabPage();
            this.lbl_gasoline_type_2 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_2 = new System.Windows.Forms.ComboBox();
            this.btn_bomb_money_2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_gallons_2 = new System.Windows.Forms.TabPage();
            this.lbl_gasoline_type_2_1 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_2_1 = new System.Windows.Forms.ComboBox();
            this.btn_bombear_galones_2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_info_2 = new System.Windows.Forms.TabPage();
            this.pbx_bomb_2 = new System.Windows.Forms.PictureBox();
            this.gbx_bomb_1 = new System.Windows.Forms.GroupBox();
            this.tab_selector_bomb_1 = new MaterialSkin.Controls.MaterialTabSelector();
            this.tab_control_bomb_1 = new MaterialSkin.Controls.MaterialTabControl();
            this.tab_page_money_1 = new System.Windows.Forms.TabPage();
            this.lbl_select_gasoline_1 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_1 = new System.Windows.Forms.ComboBox();
            this.btn_bomb_money_1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_gallons_1 = new System.Windows.Forms.TabPage();
            this.btn_bombear_galones_1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_info_1 = new System.Windows.Forms.TabPage();
            this.pbx_bomb_1 = new System.Windows.Forms.PictureBox();
            this.tab_page_bombs_4_6 = new System.Windows.Forms.TabPage();
            this.gbx_bomb_6 = new System.Windows.Forms.GroupBox();
            this.tab_selector_bomb_6 = new MaterialSkin.Controls.MaterialTabSelector();
            this.tab_control_bomb_6 = new MaterialSkin.Controls.MaterialTabControl();
            this.tab_page_money_6 = new System.Windows.Forms.TabPage();
            this.btn_bomb_money_6 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_gallons_6 = new System.Windows.Forms.TabPage();
            this.btn_bombear_galones_6 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_info_6 = new System.Windows.Forms.TabPage();
            this.pbx_bomb_6 = new System.Windows.Forms.PictureBox();
            this.gbx_bomb_5 = new System.Windows.Forms.GroupBox();
            this.tab_selector_bomb_5 = new MaterialSkin.Controls.MaterialTabSelector();
            this.tab_control_bomb_5 = new MaterialSkin.Controls.MaterialTabControl();
            this.tab_page_money_5 = new System.Windows.Forms.TabPage();
            this.btn_bomb_money_5 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_gallons_5 = new System.Windows.Forms.TabPage();
            this.btn_bombear_galones_5 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_info_5 = new System.Windows.Forms.TabPage();
            this.pbx_bomb_5 = new System.Windows.Forms.PictureBox();
            this.gbx_bomb_4 = new System.Windows.Forms.GroupBox();
            this.tab_selector_bomb_4 = new MaterialSkin.Controls.MaterialTabSelector();
            this.tab_control_bomb_4 = new MaterialSkin.Controls.MaterialTabControl();
            this.tab_page_money_4 = new System.Windows.Forms.TabPage();
            this.btn_bomb_money_4 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_gallons_4 = new System.Windows.Forms.TabPage();
            this.btn_bombear_galones_4 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tab_page_info_4 = new System.Windows.Forms.TabPage();
            this.pbx_bomb_4 = new System.Windows.Forms.PictureBox();
            this.lbl_total_diesel_b_1 = new System.Windows.Forms.Label();
            this.lbl_total_regular_b_1 = new System.Windows.Forms.Label();
            this.lbl_total_especial_b_1 = new System.Windows.Forms.Label();
            this.lbl_total_especial_b_2 = new System.Windows.Forms.Label();
            this.lbl_total_regular_b_2 = new System.Windows.Forms.Label();
            this.lbl_total_diesel_b_2 = new System.Windows.Forms.Label();
            this.lbl_total_especial_b_3 = new System.Windows.Forms.Label();
            this.lbl_total_regular_b_3 = new System.Windows.Forms.Label();
            this.lbl_total_diesel_b_3 = new System.Windows.Forms.Label();
            this.lbl_total_especial_b_4 = new System.Windows.Forms.Label();
            this.lbl_total_regular_b_4 = new System.Windows.Forms.Label();
            this.lbl_total_diesel_b_4 = new System.Windows.Forms.Label();
            this.lbl_total_especial_b_5 = new System.Windows.Forms.Label();
            this.lbl_total_regular_b_5 = new System.Windows.Forms.Label();
            this.lbl_total_diesel_b_5 = new System.Windows.Forms.Label();
            this.lbl_total_especial_b_6 = new System.Windows.Forms.Label();
            this.lbl_total_regular_b_6 = new System.Windows.Forms.Label();
            this.lbl_total_diesel_b_6 = new System.Windows.Forms.Label();
            this.txtbx_cantidad_money_1 = new System.Windows.Forms.TextBox();
            this.lbl_insert_quantity_money_1 = new MaterialSkin.Controls.MaterialLabel();
            this.lbl_insert_quantity_gallons_1 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_quantity_gallons_1 = new System.Windows.Forms.TextBox();
            this.lbl_select_gasoline_1_1 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_1_1 = new System.Windows.Forms.ComboBox();
            this.lbl_insert_quantity_money_2 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_cantidad_money_2 = new System.Windows.Forms.TextBox();
            this.lbl_insert_quantity_gallons_2 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_quantity_gallons_2 = new System.Windows.Forms.TextBox();
            this.lbl_insert_quantity_gallons_3 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_quantity_gallons_3 = new System.Windows.Forms.TextBox();
            this.lbl_insert_quantity_money_3 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_cantidad_money_3 = new System.Windows.Forms.TextBox();
            this.lbl_insert_quantity_gallons_4 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_quantity_gallons_4 = new System.Windows.Forms.TextBox();
            this.lbl_select_gasoline_4_1 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_4_1 = new System.Windows.Forms.ComboBox();
            this.lbl_insert_quantity_gallons_5 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_quantity_gallons_5 = new System.Windows.Forms.TextBox();
            this.lbl_select_gasoline_5_1 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_5_1 = new System.Windows.Forms.ComboBox();
            this.lbl_insert_quantity_gallons_6 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_quantity_gallons_6 = new System.Windows.Forms.TextBox();
            this.lbl_select_gasoline_6_1 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_6_1 = new System.Windows.Forms.ComboBox();
            this.lbl_insert_quantity_money_4 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_cantidad_money_4 = new System.Windows.Forms.TextBox();
            this.lbl_select_gasoline_4 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_4 = new System.Windows.Forms.ComboBox();
            this.lbl_insert_quantity_money_5 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_cantidad_money_5 = new System.Windows.Forms.TextBox();
            this.lbl_select_gasoline_5 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_5 = new System.Windows.Forms.ComboBox();
            this.lbl_insert_quantity_money_6 = new MaterialSkin.Controls.MaterialLabel();
            this.txtbx_cantidad_money_6 = new System.Windows.Forms.TextBox();
            this.lbl_select_gasoline_6 = new MaterialSkin.Controls.MaterialLabel();
            this.cbx_gasoline_type_6 = new System.Windows.Forms.ComboBox();
            this.tab_container_bombs.SuspendLayout();
            this.tab_page_bombs_1_3.SuspendLayout();
            this.gbx_bomb_3.SuspendLayout();
            this.tab_control_bomb_3.SuspendLayout();
            this.tab_page_money_3.SuspendLayout();
            this.tab_page_gallons_3.SuspendLayout();
            this.tab_page_info_3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_3)).BeginInit();
            this.gbx_bomb_2.SuspendLayout();
            this.tab_control_bomb_2.SuspendLayout();
            this.tab_page_bomb_2.SuspendLayout();
            this.tab_page_gallons_2.SuspendLayout();
            this.tab_page_info_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_2)).BeginInit();
            this.gbx_bomb_1.SuspendLayout();
            this.tab_control_bomb_1.SuspendLayout();
            this.tab_page_money_1.SuspendLayout();
            this.tab_page_gallons_1.SuspendLayout();
            this.tab_page_info_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_1)).BeginInit();
            this.tab_page_bombs_4_6.SuspendLayout();
            this.gbx_bomb_6.SuspendLayout();
            this.tab_control_bomb_6.SuspendLayout();
            this.tab_page_money_6.SuspendLayout();
            this.tab_page_gallons_6.SuspendLayout();
            this.tab_page_info_6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_6)).BeginInit();
            this.gbx_bomb_5.SuspendLayout();
            this.tab_control_bomb_5.SuspendLayout();
            this.tab_page_money_5.SuspendLayout();
            this.tab_page_gallons_5.SuspendLayout();
            this.tab_page_info_5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_5)).BeginInit();
            this.gbx_bomb_4.SuspendLayout();
            this.tab_control_bomb_4.SuspendLayout();
            this.tab_page_money_4.SuspendLayout();
            this.tab_page_gallons_4.SuspendLayout();
            this.tab_page_info_4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_4)).BeginInit();
            this.SuspendLayout();
            // 
            // tab_selector_bombs
            // 
            this.tab_selector_bombs.BaseTabControl = this.tab_container_bombs;
            this.tab_selector_bombs.Depth = 0;
            this.tab_selector_bombs.Location = new System.Drawing.Point(301, 64);
            this.tab_selector_bombs.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_selector_bombs.Name = "tab_selector_bombs";
            this.tab_selector_bombs.Size = new System.Drawing.Size(300, 44);
            this.tab_selector_bombs.TabIndex = 0;
            // 
            // tab_container_bombs
            // 
            this.tab_container_bombs.Controls.Add(this.tab_page_bombs_1_3);
            this.tab_container_bombs.Controls.Add(this.tab_page_bombs_4_6);
            this.tab_container_bombs.Depth = 0;
            this.tab_container_bombs.Location = new System.Drawing.Point(12, 114);
            this.tab_container_bombs.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_container_bombs.Name = "tab_container_bombs";
            this.tab_container_bombs.SelectedIndex = 0;
            this.tab_container_bombs.Size = new System.Drawing.Size(889, 470);
            this.tab_container_bombs.TabIndex = 1;
            // 
            // tab_page_bombs_1_3
            // 
            this.tab_page_bombs_1_3.Controls.Add(this.gbx_bomb_3);
            this.tab_page_bombs_1_3.Controls.Add(this.gbx_bomb_2);
            this.tab_page_bombs_1_3.Controls.Add(this.gbx_bomb_1);
            this.tab_page_bombs_1_3.Location = new System.Drawing.Point(4, 22);
            this.tab_page_bombs_1_3.Name = "tab_page_bombs_1_3";
            this.tab_page_bombs_1_3.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_bombs_1_3.Size = new System.Drawing.Size(881, 444);
            this.tab_page_bombs_1_3.TabIndex = 0;
            this.tab_page_bombs_1_3.Text = "Bombas 1 - 3";
            this.tab_page_bombs_1_3.UseVisualStyleBackColor = true;
            // 
            // gbx_bomb_3
            // 
            this.gbx_bomb_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.gbx_bomb_3.Controls.Add(this.tab_selector_bomb_3);
            this.gbx_bomb_3.Controls.Add(this.tab_control_bomb_3);
            this.gbx_bomb_3.Controls.Add(this.pbx_bomb_3);
            this.gbx_bomb_3.Location = new System.Drawing.Point(580, 18);
            this.gbx_bomb_3.Name = "gbx_bomb_3";
            this.gbx_bomb_3.Size = new System.Drawing.Size(263, 409);
            this.gbx_bomb_3.TabIndex = 11;
            this.gbx_bomb_3.TabStop = false;
            this.gbx_bomb_3.Text = "Bomba #3";
            // 
            // tab_selector_bomb_3
            // 
            this.tab_selector_bomb_3.BaseTabControl = this.tab_control_bomb_3;
            this.tab_selector_bomb_3.Depth = 0;
            this.tab_selector_bomb_3.Location = new System.Drawing.Point(0, 126);
            this.tab_selector_bomb_3.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_selector_bomb_3.Name = "tab_selector_bomb_3";
            this.tab_selector_bomb_3.Size = new System.Drawing.Size(263, 23);
            this.tab_selector_bomb_3.TabIndex = 2;
            this.tab_selector_bomb_3.Text = "materialTabSelector3";
            // 
            // tab_control_bomb_3
            // 
            this.tab_control_bomb_3.Controls.Add(this.tab_page_money_3);
            this.tab_control_bomb_3.Controls.Add(this.tab_page_gallons_3);
            this.tab_control_bomb_3.Controls.Add(this.tab_page_info_3);
            this.tab_control_bomb_3.Depth = 0;
            this.tab_control_bomb_3.Location = new System.Drawing.Point(6, 155);
            this.tab_control_bomb_3.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_control_bomb_3.Name = "tab_control_bomb_3";
            this.tab_control_bomb_3.SelectedIndex = 0;
            this.tab_control_bomb_3.Size = new System.Drawing.Size(251, 248);
            this.tab_control_bomb_3.TabIndex = 1;
            // 
            // tab_page_money_3
            // 
            this.tab_page_money_3.Controls.Add(this.lbl_insert_quantity_money_3);
            this.tab_page_money_3.Controls.Add(this.txtbx_cantidad_money_3);
            this.tab_page_money_3.Controls.Add(this.lbl_gasoline_type_3);
            this.tab_page_money_3.Controls.Add(this.cbx_gasoline_type_3);
            this.tab_page_money_3.Controls.Add(this.btn_bomb_money_3);
            this.tab_page_money_3.Location = new System.Drawing.Point(4, 22);
            this.tab_page_money_3.Name = "tab_page_money_3";
            this.tab_page_money_3.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_money_3.Size = new System.Drawing.Size(243, 222);
            this.tab_page_money_3.TabIndex = 0;
            this.tab_page_money_3.Text = "$";
            this.tab_page_money_3.UseVisualStyleBackColor = true;
            this.tab_page_money_3.Click += new System.EventHandler(this.tab_page_money_3_Click);
            // 
            // lbl_gasoline_type_3
            // 
            this.lbl_gasoline_type_3.AutoSize = true;
            this.lbl_gasoline_type_3.Depth = 0;
            this.lbl_gasoline_type_3.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_gasoline_type_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_gasoline_type_3.Location = new System.Drawing.Point(12, 21);
            this.lbl_gasoline_type_3.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_gasoline_type_3.Name = "lbl_gasoline_type_3";
            this.lbl_gasoline_type_3.Size = new System.Drawing.Size(104, 19);
            this.lbl_gasoline_type_3.TabIndex = 8;
            this.lbl_gasoline_type_3.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_3
            // 
            this.cbx_gasoline_type_3.FormattingEnabled = true;
            this.cbx_gasoline_type_3.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_3.Location = new System.Drawing.Point(13, 43);
            this.cbx_gasoline_type_3.Name = "cbx_gasoline_type_3";
            this.cbx_gasoline_type_3.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_3.TabIndex = 7;
            // 
            // btn_bomb_money_3
            // 
            this.btn_bomb_money_3.Depth = 0;
            this.btn_bomb_money_3.Location = new System.Drawing.Point(6, 210);
            this.btn_bomb_money_3.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bomb_money_3.Name = "btn_bomb_money_3";
            this.btn_bomb_money_3.Primary = true;
            this.btn_bomb_money_3.Size = new System.Drawing.Size(128, 33);
            this.btn_bomb_money_3.TabIndex = 2;
            this.btn_bomb_money_3.Text = "BOMBEAR";
            this.btn_bomb_money_3.UseVisualStyleBackColor = true;
            this.btn_bomb_money_3.Click += new System.EventHandler(this.btn_bomb_money_3_Click);
            // 
            // tab_page_gallons_3
            // 
            this.tab_page_gallons_3.Controls.Add(this.lbl_insert_quantity_gallons_3);
            this.tab_page_gallons_3.Controls.Add(this.txtbx_quantity_gallons_3);
            this.tab_page_gallons_3.Controls.Add(this.lbl_gasoline_type_3_1);
            this.tab_page_gallons_3.Controls.Add(this.cbx_gasoline_type_3_1);
            this.tab_page_gallons_3.Controls.Add(this.btn_bombear_galones_3);
            this.tab_page_gallons_3.Location = new System.Drawing.Point(4, 22);
            this.tab_page_gallons_3.Name = "tab_page_gallons_3";
            this.tab_page_gallons_3.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_gallons_3.Size = new System.Drawing.Size(243, 222);
            this.tab_page_gallons_3.TabIndex = 1;
            this.tab_page_gallons_3.Text = "Galónes";
            this.tab_page_gallons_3.UseVisualStyleBackColor = true;
            this.tab_page_gallons_3.Click += new System.EventHandler(this.tab_page_gallons_3_Click);
            // 
            // lbl_gasoline_type_3_1
            // 
            this.lbl_gasoline_type_3_1.AccessibleDescription = "";
            this.lbl_gasoline_type_3_1.AutoSize = true;
            this.lbl_gasoline_type_3_1.Depth = 0;
            this.lbl_gasoline_type_3_1.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_gasoline_type_3_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_gasoline_type_3_1.Location = new System.Drawing.Point(12, 21);
            this.lbl_gasoline_type_3_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_gasoline_type_3_1.Name = "lbl_gasoline_type_3_1";
            this.lbl_gasoline_type_3_1.Size = new System.Drawing.Size(104, 19);
            this.lbl_gasoline_type_3_1.TabIndex = 10;
            this.lbl_gasoline_type_3_1.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_3_1
            // 
            this.cbx_gasoline_type_3_1.FormattingEnabled = true;
            this.cbx_gasoline_type_3_1.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_3_1.Location = new System.Drawing.Point(13, 43);
            this.cbx_gasoline_type_3_1.Name = "cbx_gasoline_type_3_1";
            this.cbx_gasoline_type_3_1.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_3_1.TabIndex = 9;
            // 
            // btn_bombear_galones_3
            // 
            this.btn_bombear_galones_3.Depth = 0;
            this.btn_bombear_galones_3.Location = new System.Drawing.Point(109, 210);
            this.btn_bombear_galones_3.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bombear_galones_3.Name = "btn_bombear_galones_3";
            this.btn_bombear_galones_3.Primary = true;
            this.btn_bombear_galones_3.Size = new System.Drawing.Size(128, 33);
            this.btn_bombear_galones_3.TabIndex = 2;
            this.btn_bombear_galones_3.Text = "BOMBEAR";
            this.btn_bombear_galones_3.UseVisualStyleBackColor = true;
            this.btn_bombear_galones_3.Click += new System.EventHandler(this.btn_bombear_galones_3_Click);
            // 
            // tab_page_info_3
            // 
            this.tab_page_info_3.Controls.Add(this.lbl_total_especial_b_3);
            this.tab_page_info_3.Controls.Add(this.lbl_total_regular_b_3);
            this.tab_page_info_3.Controls.Add(this.lbl_total_diesel_b_3);
            this.tab_page_info_3.Location = new System.Drawing.Point(4, 22);
            this.tab_page_info_3.Name = "tab_page_info_3";
            this.tab_page_info_3.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_info_3.Size = new System.Drawing.Size(243, 222);
            this.tab_page_info_3.TabIndex = 2;
            this.tab_page_info_3.Text = "Info";
            this.tab_page_info_3.UseVisualStyleBackColor = true;
            // 
            // pbx_bomb_3
            // 
            this.pbx_bomb_3.Image = ((System.Drawing.Image)(resources.GetObject("pbx_bomb_3.Image")));
            this.pbx_bomb_3.Location = new System.Drawing.Point(98, 36);
            this.pbx_bomb_3.Name = "pbx_bomb_3";
            this.pbx_bomb_3.Size = new System.Drawing.Size(60, 60);
            this.pbx_bomb_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_bomb_3.TabIndex = 0;
            this.pbx_bomb_3.TabStop = false;
            // 
            // gbx_bomb_2
            // 
            this.gbx_bomb_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.gbx_bomb_2.Controls.Add(this.tab_selector_bomb_2);
            this.gbx_bomb_2.Controls.Add(this.tab_control_bomb_2);
            this.gbx_bomb_2.Controls.Add(this.pbx_bomb_2);
            this.gbx_bomb_2.Location = new System.Drawing.Point(311, 18);
            this.gbx_bomb_2.Name = "gbx_bomb_2";
            this.gbx_bomb_2.Size = new System.Drawing.Size(263, 409);
            this.gbx_bomb_2.TabIndex = 10;
            this.gbx_bomb_2.TabStop = false;
            this.gbx_bomb_2.Text = "Bomba #2";
            this.gbx_bomb_2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // tab_selector_bomb_2
            // 
            this.tab_selector_bomb_2.BaseTabControl = this.tab_control_bomb_2;
            this.tab_selector_bomb_2.Depth = 0;
            this.tab_selector_bomb_2.Location = new System.Drawing.Point(0, 126);
            this.tab_selector_bomb_2.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_selector_bomb_2.Name = "tab_selector_bomb_2";
            this.tab_selector_bomb_2.Size = new System.Drawing.Size(263, 23);
            this.tab_selector_bomb_2.TabIndex = 2;
            // 
            // tab_control_bomb_2
            // 
            this.tab_control_bomb_2.Controls.Add(this.tab_page_bomb_2);
            this.tab_control_bomb_2.Controls.Add(this.tab_page_gallons_2);
            this.tab_control_bomb_2.Controls.Add(this.tab_page_info_2);
            this.tab_control_bomb_2.Depth = 0;
            this.tab_control_bomb_2.Location = new System.Drawing.Point(6, 155);
            this.tab_control_bomb_2.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_control_bomb_2.Name = "tab_control_bomb_2";
            this.tab_control_bomb_2.SelectedIndex = 0;
            this.tab_control_bomb_2.Size = new System.Drawing.Size(251, 248);
            this.tab_control_bomb_2.TabIndex = 1;
            // 
            // tab_page_bomb_2
            // 
            this.tab_page_bomb_2.Controls.Add(this.lbl_insert_quantity_money_2);
            this.tab_page_bomb_2.Controls.Add(this.txtbx_cantidad_money_2);
            this.tab_page_bomb_2.Controls.Add(this.lbl_gasoline_type_2);
            this.tab_page_bomb_2.Controls.Add(this.cbx_gasoline_type_2);
            this.tab_page_bomb_2.Controls.Add(this.btn_bomb_money_2);
            this.tab_page_bomb_2.Location = new System.Drawing.Point(4, 22);
            this.tab_page_bomb_2.Name = "tab_page_bomb_2";
            this.tab_page_bomb_2.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_bomb_2.Size = new System.Drawing.Size(243, 222);
            this.tab_page_bomb_2.TabIndex = 0;
            this.tab_page_bomb_2.Text = "$";
            this.tab_page_bomb_2.UseVisualStyleBackColor = true;
            // 
            // lbl_gasoline_type_2
            // 
            this.lbl_gasoline_type_2.AutoSize = true;
            this.lbl_gasoline_type_2.Depth = 0;
            this.lbl_gasoline_type_2.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_gasoline_type_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_gasoline_type_2.Location = new System.Drawing.Point(12, 21);
            this.lbl_gasoline_type_2.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_gasoline_type_2.Name = "lbl_gasoline_type_2";
            this.lbl_gasoline_type_2.Size = new System.Drawing.Size(104, 19);
            this.lbl_gasoline_type_2.TabIndex = 8;
            this.lbl_gasoline_type_2.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_2
            // 
            this.cbx_gasoline_type_2.FormattingEnabled = true;
            this.cbx_gasoline_type_2.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_2.Location = new System.Drawing.Point(13, 43);
            this.cbx_gasoline_type_2.Name = "cbx_gasoline_type_2";
            this.cbx_gasoline_type_2.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_2.TabIndex = 7;
            // 
            // btn_bomb_money_2
            // 
            this.btn_bomb_money_2.Depth = 0;
            this.btn_bomb_money_2.Location = new System.Drawing.Point(6, 210);
            this.btn_bomb_money_2.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bomb_money_2.Name = "btn_bomb_money_2";
            this.btn_bomb_money_2.Primary = true;
            this.btn_bomb_money_2.Size = new System.Drawing.Size(128, 33);
            this.btn_bomb_money_2.TabIndex = 1;
            this.btn_bomb_money_2.Text = "BOMBEAR";
            this.btn_bomb_money_2.UseVisualStyleBackColor = true;
            this.btn_bomb_money_2.Click += new System.EventHandler(this.btn_bomb_money_2_Click);
            // 
            // tab_page_gallons_2
            // 
            this.tab_page_gallons_2.Controls.Add(this.lbl_insert_quantity_gallons_2);
            this.tab_page_gallons_2.Controls.Add(this.txtbx_quantity_gallons_2);
            this.tab_page_gallons_2.Controls.Add(this.lbl_gasoline_type_2_1);
            this.tab_page_gallons_2.Controls.Add(this.cbx_gasoline_type_2_1);
            this.tab_page_gallons_2.Controls.Add(this.btn_bombear_galones_2);
            this.tab_page_gallons_2.Location = new System.Drawing.Point(4, 22);
            this.tab_page_gallons_2.Name = "tab_page_gallons_2";
            this.tab_page_gallons_2.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_gallons_2.Size = new System.Drawing.Size(243, 222);
            this.tab_page_gallons_2.TabIndex = 1;
            this.tab_page_gallons_2.Text = "Galónes";
            this.tab_page_gallons_2.UseVisualStyleBackColor = true;
            // 
            // lbl_gasoline_type_2_1
            // 
            this.lbl_gasoline_type_2_1.AutoSize = true;
            this.lbl_gasoline_type_2_1.Depth = 0;
            this.lbl_gasoline_type_2_1.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_gasoline_type_2_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_gasoline_type_2_1.Location = new System.Drawing.Point(12, 21);
            this.lbl_gasoline_type_2_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_gasoline_type_2_1.Name = "lbl_gasoline_type_2_1";
            this.lbl_gasoline_type_2_1.Size = new System.Drawing.Size(104, 19);
            this.lbl_gasoline_type_2_1.TabIndex = 6;
            this.lbl_gasoline_type_2_1.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_2_1
            // 
            this.cbx_gasoline_type_2_1.FormattingEnabled = true;
            this.cbx_gasoline_type_2_1.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_2_1.Location = new System.Drawing.Point(13, 43);
            this.cbx_gasoline_type_2_1.Name = "cbx_gasoline_type_2_1";
            this.cbx_gasoline_type_2_1.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_2_1.TabIndex = 5;
            // 
            // btn_bombear_galones_2
            // 
            this.btn_bombear_galones_2.Depth = 0;
            this.btn_bombear_galones_2.Location = new System.Drawing.Point(109, 210);
            this.btn_bombear_galones_2.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bombear_galones_2.Name = "btn_bombear_galones_2";
            this.btn_bombear_galones_2.Primary = true;
            this.btn_bombear_galones_2.Size = new System.Drawing.Size(128, 33);
            this.btn_bombear_galones_2.TabIndex = 2;
            this.btn_bombear_galones_2.Text = "BOMBEAR";
            this.btn_bombear_galones_2.UseVisualStyleBackColor = true;
            this.btn_bombear_galones_2.Click += new System.EventHandler(this.btn_bombear_galones_2_Click);
            // 
            // tab_page_info_2
            // 
            this.tab_page_info_2.Controls.Add(this.lbl_total_especial_b_2);
            this.tab_page_info_2.Controls.Add(this.lbl_total_regular_b_2);
            this.tab_page_info_2.Controls.Add(this.lbl_total_diesel_b_2);
            this.tab_page_info_2.Location = new System.Drawing.Point(4, 22);
            this.tab_page_info_2.Name = "tab_page_info_2";
            this.tab_page_info_2.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_info_2.Size = new System.Drawing.Size(243, 222);
            this.tab_page_info_2.TabIndex = 2;
            this.tab_page_info_2.Text = "Info";
            this.tab_page_info_2.UseVisualStyleBackColor = true;
            this.tab_page_info_2.Click += new System.EventHandler(this.tab_page_info_2_Click);
            // 
            // pbx_bomb_2
            // 
            this.pbx_bomb_2.Image = ((System.Drawing.Image)(resources.GetObject("pbx_bomb_2.Image")));
            this.pbx_bomb_2.Location = new System.Drawing.Point(98, 36);
            this.pbx_bomb_2.Name = "pbx_bomb_2";
            this.pbx_bomb_2.Size = new System.Drawing.Size(60, 60);
            this.pbx_bomb_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_bomb_2.TabIndex = 0;
            this.pbx_bomb_2.TabStop = false;
            // 
            // gbx_bomb_1
            // 
            this.gbx_bomb_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.gbx_bomb_1.Controls.Add(this.tab_selector_bomb_1);
            this.gbx_bomb_1.Controls.Add(this.tab_control_bomb_1);
            this.gbx_bomb_1.Controls.Add(this.pbx_bomb_1);
            this.gbx_bomb_1.Location = new System.Drawing.Point(42, 18);
            this.gbx_bomb_1.Name = "gbx_bomb_1";
            this.gbx_bomb_1.Size = new System.Drawing.Size(263, 409);
            this.gbx_bomb_1.TabIndex = 9;
            this.gbx_bomb_1.TabStop = false;
            this.gbx_bomb_1.Text = "Bomba #1";
            // 
            // tab_selector_bomb_1
            // 
            this.tab_selector_bomb_1.BaseTabControl = this.tab_control_bomb_1;
            this.tab_selector_bomb_1.Depth = 0;
            this.tab_selector_bomb_1.Location = new System.Drawing.Point(0, 126);
            this.tab_selector_bomb_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_selector_bomb_1.Name = "tab_selector_bomb_1";
            this.tab_selector_bomb_1.Size = new System.Drawing.Size(263, 23);
            this.tab_selector_bomb_1.TabIndex = 2;
            // 
            // tab_control_bomb_1
            // 
            this.tab_control_bomb_1.Controls.Add(this.tab_page_money_1);
            this.tab_control_bomb_1.Controls.Add(this.tab_page_gallons_1);
            this.tab_control_bomb_1.Controls.Add(this.tab_page_info_1);
            this.tab_control_bomb_1.Depth = 0;
            this.tab_control_bomb_1.Location = new System.Drawing.Point(6, 155);
            this.tab_control_bomb_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_control_bomb_1.Name = "tab_control_bomb_1";
            this.tab_control_bomb_1.SelectedIndex = 0;
            this.tab_control_bomb_1.Size = new System.Drawing.Size(251, 248);
            this.tab_control_bomb_1.TabIndex = 1;
            // 
            // tab_page_money_1
            // 
            this.tab_page_money_1.Controls.Add(this.lbl_insert_quantity_money_1);
            this.tab_page_money_1.Controls.Add(this.txtbx_cantidad_money_1);
            this.tab_page_money_1.Controls.Add(this.lbl_select_gasoline_1);
            this.tab_page_money_1.Controls.Add(this.cbx_gasoline_type_1);
            this.tab_page_money_1.Controls.Add(this.btn_bomb_money_1);
            this.tab_page_money_1.Location = new System.Drawing.Point(4, 22);
            this.tab_page_money_1.Name = "tab_page_money_1";
            this.tab_page_money_1.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_money_1.Size = new System.Drawing.Size(243, 222);
            this.tab_page_money_1.TabIndex = 0;
            this.tab_page_money_1.Text = "$";
            this.tab_page_money_1.UseVisualStyleBackColor = true;
            // 
            // lbl_select_gasoline_1
            // 
            this.lbl_select_gasoline_1.AutoSize = true;
            this.lbl_select_gasoline_1.Depth = 0;
            this.lbl_select_gasoline_1.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_select_gasoline_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_select_gasoline_1.Location = new System.Drawing.Point(12, 21);
            this.lbl_select_gasoline_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_select_gasoline_1.Name = "lbl_select_gasoline_1";
            this.lbl_select_gasoline_1.Size = new System.Drawing.Size(104, 19);
            this.lbl_select_gasoline_1.TabIndex = 2;
            this.lbl_select_gasoline_1.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_1
            // 
            this.cbx_gasoline_type_1.FormattingEnabled = true;
            this.cbx_gasoline_type_1.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_1.Location = new System.Drawing.Point(13, 43);
            this.cbx_gasoline_type_1.Name = "cbx_gasoline_type_1";
            this.cbx_gasoline_type_1.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_1.TabIndex = 1;
            // 
            // btn_bomb_money_1
            // 
            this.btn_bomb_money_1.Depth = 0;
            this.btn_bomb_money_1.Location = new System.Drawing.Point(6, 210);
            this.btn_bomb_money_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bomb_money_1.Name = "btn_bomb_money_1";
            this.btn_bomb_money_1.Primary = true;
            this.btn_bomb_money_1.Size = new System.Drawing.Size(128, 33);
            this.btn_bomb_money_1.TabIndex = 0;
            this.btn_bomb_money_1.Text = "BOMBEAR";
            this.btn_bomb_money_1.UseVisualStyleBackColor = true;
            this.btn_bomb_money_1.Click += new System.EventHandler(this.btn_bomb_money_1_Click);
            // 
            // tab_page_gallons_1
            // 
            this.tab_page_gallons_1.Controls.Add(this.lbl_insert_quantity_gallons_1);
            this.tab_page_gallons_1.Controls.Add(this.txtbx_quantity_gallons_1);
            this.tab_page_gallons_1.Controls.Add(this.lbl_select_gasoline_1_1);
            this.tab_page_gallons_1.Controls.Add(this.cbx_gasoline_type_1_1);
            this.tab_page_gallons_1.Controls.Add(this.btn_bombear_galones_1);
            this.tab_page_gallons_1.Location = new System.Drawing.Point(4, 22);
            this.tab_page_gallons_1.Name = "tab_page_gallons_1";
            this.tab_page_gallons_1.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_gallons_1.Size = new System.Drawing.Size(243, 222);
            this.tab_page_gallons_1.TabIndex = 1;
            this.tab_page_gallons_1.Text = "Galónes";
            this.tab_page_gallons_1.UseVisualStyleBackColor = true;
            // 
            // btn_bombear_galones_1
            // 
            this.btn_bombear_galones_1.Depth = 0;
            this.btn_bombear_galones_1.Location = new System.Drawing.Point(109, 210);
            this.btn_bombear_galones_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bombear_galones_1.Name = "btn_bombear_galones_1";
            this.btn_bombear_galones_1.Primary = true;
            this.btn_bombear_galones_1.Size = new System.Drawing.Size(128, 33);
            this.btn_bombear_galones_1.TabIndex = 1;
            this.btn_bombear_galones_1.Text = "BOMBEAR";
            this.btn_bombear_galones_1.UseVisualStyleBackColor = true;
            this.btn_bombear_galones_1.Click += new System.EventHandler(this.btn_bombear_galones_1_Click);
            // 
            // tab_page_info_1
            // 
            this.tab_page_info_1.Controls.Add(this.lbl_total_especial_b_1);
            this.tab_page_info_1.Controls.Add(this.lbl_total_regular_b_1);
            this.tab_page_info_1.Controls.Add(this.lbl_total_diesel_b_1);
            this.tab_page_info_1.Location = new System.Drawing.Point(4, 22);
            this.tab_page_info_1.Name = "tab_page_info_1";
            this.tab_page_info_1.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_info_1.Size = new System.Drawing.Size(243, 222);
            this.tab_page_info_1.TabIndex = 2;
            this.tab_page_info_1.Text = "Info";
            this.tab_page_info_1.UseVisualStyleBackColor = true;
            this.tab_page_info_1.Click += new System.EventHandler(this.tab_page_info_1_Click);
            // 
            // pbx_bomb_1
            // 
            this.pbx_bomb_1.Image = ((System.Drawing.Image)(resources.GetObject("pbx_bomb_1.Image")));
            this.pbx_bomb_1.Location = new System.Drawing.Point(98, 36);
            this.pbx_bomb_1.Name = "pbx_bomb_1";
            this.pbx_bomb_1.Size = new System.Drawing.Size(60, 60);
            this.pbx_bomb_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_bomb_1.TabIndex = 0;
            this.pbx_bomb_1.TabStop = false;
            // 
            // tab_page_bombs_4_6
            // 
            this.tab_page_bombs_4_6.Controls.Add(this.gbx_bomb_6);
            this.tab_page_bombs_4_6.Controls.Add(this.gbx_bomb_5);
            this.tab_page_bombs_4_6.Controls.Add(this.gbx_bomb_4);
            this.tab_page_bombs_4_6.Location = new System.Drawing.Point(4, 22);
            this.tab_page_bombs_4_6.Name = "tab_page_bombs_4_6";
            this.tab_page_bombs_4_6.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_bombs_4_6.Size = new System.Drawing.Size(881, 444);
            this.tab_page_bombs_4_6.TabIndex = 1;
            this.tab_page_bombs_4_6.Text = "Bombas 4 - 6";
            this.tab_page_bombs_4_6.UseVisualStyleBackColor = true;
            // 
            // gbx_bomb_6
            // 
            this.gbx_bomb_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.gbx_bomb_6.Controls.Add(this.tab_selector_bomb_6);
            this.gbx_bomb_6.Controls.Add(this.tab_control_bomb_6);
            this.gbx_bomb_6.Controls.Add(this.pbx_bomb_6);
            this.gbx_bomb_6.Location = new System.Drawing.Point(580, 18);
            this.gbx_bomb_6.Name = "gbx_bomb_6";
            this.gbx_bomb_6.Size = new System.Drawing.Size(263, 409);
            this.gbx_bomb_6.TabIndex = 14;
            this.gbx_bomb_6.TabStop = false;
            this.gbx_bomb_6.Text = "Bomba #6";
            // 
            // tab_selector_bomb_6
            // 
            this.tab_selector_bomb_6.BaseTabControl = this.tab_control_bomb_6;
            this.tab_selector_bomb_6.Depth = 0;
            this.tab_selector_bomb_6.Location = new System.Drawing.Point(0, 126);
            this.tab_selector_bomb_6.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_selector_bomb_6.Name = "tab_selector_bomb_6";
            this.tab_selector_bomb_6.Size = new System.Drawing.Size(263, 23);
            this.tab_selector_bomb_6.TabIndex = 2;
            // 
            // tab_control_bomb_6
            // 
            this.tab_control_bomb_6.Controls.Add(this.tab_page_money_6);
            this.tab_control_bomb_6.Controls.Add(this.tab_page_gallons_6);
            this.tab_control_bomb_6.Controls.Add(this.tab_page_info_6);
            this.tab_control_bomb_6.Depth = 0;
            this.tab_control_bomb_6.Location = new System.Drawing.Point(6, 155);
            this.tab_control_bomb_6.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_control_bomb_6.Name = "tab_control_bomb_6";
            this.tab_control_bomb_6.SelectedIndex = 0;
            this.tab_control_bomb_6.Size = new System.Drawing.Size(251, 248);
            this.tab_control_bomb_6.TabIndex = 1;
            // 
            // tab_page_money_6
            // 
            this.tab_page_money_6.Controls.Add(this.lbl_insert_quantity_money_6);
            this.tab_page_money_6.Controls.Add(this.txtbx_cantidad_money_6);
            this.tab_page_money_6.Controls.Add(this.lbl_select_gasoline_6);
            this.tab_page_money_6.Controls.Add(this.cbx_gasoline_type_6);
            this.tab_page_money_6.Controls.Add(this.btn_bomb_money_6);
            this.tab_page_money_6.Location = new System.Drawing.Point(4, 22);
            this.tab_page_money_6.Name = "tab_page_money_6";
            this.tab_page_money_6.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_money_6.Size = new System.Drawing.Size(243, 222);
            this.tab_page_money_6.TabIndex = 0;
            this.tab_page_money_6.Text = "$";
            this.tab_page_money_6.UseVisualStyleBackColor = true;
            // 
            // btn_bomb_money_6
            // 
            this.btn_bomb_money_6.Depth = 0;
            this.btn_bomb_money_6.Location = new System.Drawing.Point(6, 210);
            this.btn_bomb_money_6.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bomb_money_6.Name = "btn_bomb_money_6";
            this.btn_bomb_money_6.Primary = true;
            this.btn_bomb_money_6.Size = new System.Drawing.Size(128, 33);
            this.btn_bomb_money_6.TabIndex = 3;
            this.btn_bomb_money_6.Text = "BOMBEAR";
            this.btn_bomb_money_6.UseVisualStyleBackColor = true;
            this.btn_bomb_money_6.Click += new System.EventHandler(this.btn_bomb_money_6_Click);
            // 
            // tab_page_gallons_6
            // 
            this.tab_page_gallons_6.Controls.Add(this.lbl_insert_quantity_gallons_6);
            this.tab_page_gallons_6.Controls.Add(this.txtbx_quantity_gallons_6);
            this.tab_page_gallons_6.Controls.Add(this.lbl_select_gasoline_6_1);
            this.tab_page_gallons_6.Controls.Add(this.cbx_gasoline_type_6_1);
            this.tab_page_gallons_6.Controls.Add(this.btn_bombear_galones_6);
            this.tab_page_gallons_6.Location = new System.Drawing.Point(4, 22);
            this.tab_page_gallons_6.Name = "tab_page_gallons_6";
            this.tab_page_gallons_6.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_gallons_6.Size = new System.Drawing.Size(243, 222);
            this.tab_page_gallons_6.TabIndex = 1;
            this.tab_page_gallons_6.Text = "Galónes";
            this.tab_page_gallons_6.UseVisualStyleBackColor = true;
            // 
            // btn_bombear_galones_6
            // 
            this.btn_bombear_galones_6.Depth = 0;
            this.btn_bombear_galones_6.Location = new System.Drawing.Point(112, 210);
            this.btn_bombear_galones_6.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bombear_galones_6.Name = "btn_bombear_galones_6";
            this.btn_bombear_galones_6.Primary = true;
            this.btn_bombear_galones_6.Size = new System.Drawing.Size(128, 33);
            this.btn_bombear_galones_6.TabIndex = 4;
            this.btn_bombear_galones_6.Text = "BOMBEAR";
            this.btn_bombear_galones_6.UseVisualStyleBackColor = true;
            this.btn_bombear_galones_6.Click += new System.EventHandler(this.btn_bombear_galones_6_Click);
            // 
            // tab_page_info_6
            // 
            this.tab_page_info_6.Controls.Add(this.lbl_total_especial_b_6);
            this.tab_page_info_6.Controls.Add(this.lbl_total_diesel_b_6);
            this.tab_page_info_6.Controls.Add(this.lbl_total_regular_b_6);
            this.tab_page_info_6.Location = new System.Drawing.Point(4, 22);
            this.tab_page_info_6.Name = "tab_page_info_6";
            this.tab_page_info_6.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_info_6.Size = new System.Drawing.Size(243, 222);
            this.tab_page_info_6.TabIndex = 2;
            this.tab_page_info_6.Text = "Info";
            this.tab_page_info_6.UseVisualStyleBackColor = true;
            // 
            // pbx_bomb_6
            // 
            this.pbx_bomb_6.Image = ((System.Drawing.Image)(resources.GetObject("pbx_bomb_6.Image")));
            this.pbx_bomb_6.Location = new System.Drawing.Point(98, 36);
            this.pbx_bomb_6.Name = "pbx_bomb_6";
            this.pbx_bomb_6.Size = new System.Drawing.Size(60, 60);
            this.pbx_bomb_6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_bomb_6.TabIndex = 0;
            this.pbx_bomb_6.TabStop = false;
            // 
            // gbx_bomb_5
            // 
            this.gbx_bomb_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.gbx_bomb_5.Controls.Add(this.tab_selector_bomb_5);
            this.gbx_bomb_5.Controls.Add(this.tab_control_bomb_5);
            this.gbx_bomb_5.Controls.Add(this.pbx_bomb_5);
            this.gbx_bomb_5.Location = new System.Drawing.Point(311, 18);
            this.gbx_bomb_5.Name = "gbx_bomb_5";
            this.gbx_bomb_5.Size = new System.Drawing.Size(263, 409);
            this.gbx_bomb_5.TabIndex = 13;
            this.gbx_bomb_5.TabStop = false;
            this.gbx_bomb_5.Text = "Bomba #5";
            // 
            // tab_selector_bomb_5
            // 
            this.tab_selector_bomb_5.BaseTabControl = this.tab_control_bomb_5;
            this.tab_selector_bomb_5.Depth = 0;
            this.tab_selector_bomb_5.Location = new System.Drawing.Point(0, 126);
            this.tab_selector_bomb_5.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_selector_bomb_5.Name = "tab_selector_bomb_5";
            this.tab_selector_bomb_5.Size = new System.Drawing.Size(263, 23);
            this.tab_selector_bomb_5.TabIndex = 2;
            // 
            // tab_control_bomb_5
            // 
            this.tab_control_bomb_5.Controls.Add(this.tab_page_money_5);
            this.tab_control_bomb_5.Controls.Add(this.tab_page_gallons_5);
            this.tab_control_bomb_5.Controls.Add(this.tab_page_info_5);
            this.tab_control_bomb_5.Depth = 0;
            this.tab_control_bomb_5.Location = new System.Drawing.Point(6, 155);
            this.tab_control_bomb_5.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_control_bomb_5.Name = "tab_control_bomb_5";
            this.tab_control_bomb_5.SelectedIndex = 0;
            this.tab_control_bomb_5.Size = new System.Drawing.Size(251, 248);
            this.tab_control_bomb_5.TabIndex = 1;
            // 
            // tab_page_money_5
            // 
            this.tab_page_money_5.Controls.Add(this.lbl_insert_quantity_money_5);
            this.tab_page_money_5.Controls.Add(this.txtbx_cantidad_money_5);
            this.tab_page_money_5.Controls.Add(this.lbl_select_gasoline_5);
            this.tab_page_money_5.Controls.Add(this.cbx_gasoline_type_5);
            this.tab_page_money_5.Controls.Add(this.btn_bomb_money_5);
            this.tab_page_money_5.Location = new System.Drawing.Point(4, 22);
            this.tab_page_money_5.Name = "tab_page_money_5";
            this.tab_page_money_5.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_money_5.Size = new System.Drawing.Size(243, 222);
            this.tab_page_money_5.TabIndex = 0;
            this.tab_page_money_5.Text = "$";
            this.tab_page_money_5.UseVisualStyleBackColor = true;
            // 
            // btn_bomb_money_5
            // 
            this.btn_bomb_money_5.Depth = 0;
            this.btn_bomb_money_5.Location = new System.Drawing.Point(6, 210);
            this.btn_bomb_money_5.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bomb_money_5.Name = "btn_bomb_money_5";
            this.btn_bomb_money_5.Primary = true;
            this.btn_bomb_money_5.Size = new System.Drawing.Size(128, 33);
            this.btn_bomb_money_5.TabIndex = 4;
            this.btn_bomb_money_5.Text = "BOMBEAR";
            this.btn_bomb_money_5.UseVisualStyleBackColor = true;
            this.btn_bomb_money_5.Click += new System.EventHandler(this.btn_bomb_money_5_Click);
            // 
            // tab_page_gallons_5
            // 
            this.tab_page_gallons_5.Controls.Add(this.lbl_insert_quantity_gallons_5);
            this.tab_page_gallons_5.Controls.Add(this.txtbx_quantity_gallons_5);
            this.tab_page_gallons_5.Controls.Add(this.lbl_select_gasoline_5_1);
            this.tab_page_gallons_5.Controls.Add(this.cbx_gasoline_type_5_1);
            this.tab_page_gallons_5.Controls.Add(this.btn_bombear_galones_5);
            this.tab_page_gallons_5.Location = new System.Drawing.Point(4, 22);
            this.tab_page_gallons_5.Name = "tab_page_gallons_5";
            this.tab_page_gallons_5.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_gallons_5.Size = new System.Drawing.Size(243, 222);
            this.tab_page_gallons_5.TabIndex = 1;
            this.tab_page_gallons_5.Text = "Galónes";
            this.tab_page_gallons_5.UseVisualStyleBackColor = true;
            this.tab_page_gallons_5.Click += new System.EventHandler(this.tab_page_gallons_5_Click);
            // 
            // btn_bombear_galones_5
            // 
            this.btn_bombear_galones_5.Depth = 0;
            this.btn_bombear_galones_5.Location = new System.Drawing.Point(112, 210);
            this.btn_bombear_galones_5.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bombear_galones_5.Name = "btn_bombear_galones_5";
            this.btn_bombear_galones_5.Primary = true;
            this.btn_bombear_galones_5.Size = new System.Drawing.Size(128, 33);
            this.btn_bombear_galones_5.TabIndex = 5;
            this.btn_bombear_galones_5.Text = "BOMBEAR";
            this.btn_bombear_galones_5.UseVisualStyleBackColor = true;
            this.btn_bombear_galones_5.Click += new System.EventHandler(this.btn_bombear_galones_5_Click);
            // 
            // tab_page_info_5
            // 
            this.tab_page_info_5.Controls.Add(this.lbl_total_especial_b_5);
            this.tab_page_info_5.Controls.Add(this.lbl_total_regular_b_5);
            this.tab_page_info_5.Controls.Add(this.lbl_total_diesel_b_5);
            this.tab_page_info_5.Location = new System.Drawing.Point(4, 22);
            this.tab_page_info_5.Name = "tab_page_info_5";
            this.tab_page_info_5.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_info_5.Size = new System.Drawing.Size(243, 222);
            this.tab_page_info_5.TabIndex = 2;
            this.tab_page_info_5.Text = "Info";
            this.tab_page_info_5.UseVisualStyleBackColor = true;
            // 
            // pbx_bomb_5
            // 
            this.pbx_bomb_5.Image = ((System.Drawing.Image)(resources.GetObject("pbx_bomb_5.Image")));
            this.pbx_bomb_5.Location = new System.Drawing.Point(98, 36);
            this.pbx_bomb_5.Name = "pbx_bomb_5";
            this.pbx_bomb_5.Size = new System.Drawing.Size(60, 60);
            this.pbx_bomb_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_bomb_5.TabIndex = 0;
            this.pbx_bomb_5.TabStop = false;
            // 
            // gbx_bomb_4
            // 
            this.gbx_bomb_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(240)))), ((int)(((byte)(241)))));
            this.gbx_bomb_4.Controls.Add(this.tab_selector_bomb_4);
            this.gbx_bomb_4.Controls.Add(this.tab_control_bomb_4);
            this.gbx_bomb_4.Controls.Add(this.pbx_bomb_4);
            this.gbx_bomb_4.Location = new System.Drawing.Point(42, 18);
            this.gbx_bomb_4.Name = "gbx_bomb_4";
            this.gbx_bomb_4.Size = new System.Drawing.Size(263, 409);
            this.gbx_bomb_4.TabIndex = 12;
            this.gbx_bomb_4.TabStop = false;
            this.gbx_bomb_4.Text = "Bomba #4";
            // 
            // tab_selector_bomb_4
            // 
            this.tab_selector_bomb_4.BaseTabControl = this.tab_control_bomb_4;
            this.tab_selector_bomb_4.Depth = 0;
            this.tab_selector_bomb_4.Location = new System.Drawing.Point(0, 126);
            this.tab_selector_bomb_4.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_selector_bomb_4.Name = "tab_selector_bomb_4";
            this.tab_selector_bomb_4.Size = new System.Drawing.Size(263, 23);
            this.tab_selector_bomb_4.TabIndex = 2;
            // 
            // tab_control_bomb_4
            // 
            this.tab_control_bomb_4.Controls.Add(this.tab_page_money_4);
            this.tab_control_bomb_4.Controls.Add(this.tab_page_gallons_4);
            this.tab_control_bomb_4.Controls.Add(this.tab_page_info_4);
            this.tab_control_bomb_4.Depth = 0;
            this.tab_control_bomb_4.Location = new System.Drawing.Point(6, 155);
            this.tab_control_bomb_4.MouseState = MaterialSkin.MouseState.HOVER;
            this.tab_control_bomb_4.Name = "tab_control_bomb_4";
            this.tab_control_bomb_4.SelectedIndex = 0;
            this.tab_control_bomb_4.Size = new System.Drawing.Size(251, 248);
            this.tab_control_bomb_4.TabIndex = 1;
            // 
            // tab_page_money_4
            // 
            this.tab_page_money_4.Controls.Add(this.lbl_insert_quantity_money_4);
            this.tab_page_money_4.Controls.Add(this.txtbx_cantidad_money_4);
            this.tab_page_money_4.Controls.Add(this.lbl_select_gasoline_4);
            this.tab_page_money_4.Controls.Add(this.cbx_gasoline_type_4);
            this.tab_page_money_4.Controls.Add(this.btn_bomb_money_4);
            this.tab_page_money_4.Location = new System.Drawing.Point(4, 22);
            this.tab_page_money_4.Name = "tab_page_money_4";
            this.tab_page_money_4.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_money_4.Size = new System.Drawing.Size(243, 222);
            this.tab_page_money_4.TabIndex = 0;
            this.tab_page_money_4.Text = "$";
            this.tab_page_money_4.UseVisualStyleBackColor = true;
            // 
            // btn_bomb_money_4
            // 
            this.btn_bomb_money_4.Depth = 0;
            this.btn_bomb_money_4.Location = new System.Drawing.Point(6, 210);
            this.btn_bomb_money_4.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bomb_money_4.Name = "btn_bomb_money_4";
            this.btn_bomb_money_4.Primary = true;
            this.btn_bomb_money_4.Size = new System.Drawing.Size(128, 33);
            this.btn_bomb_money_4.TabIndex = 2;
            this.btn_bomb_money_4.Text = "BOMBEAR";
            this.btn_bomb_money_4.UseVisualStyleBackColor = true;
            this.btn_bomb_money_4.Click += new System.EventHandler(this.btn_bomb_money_4_Click);
            // 
            // tab_page_gallons_4
            // 
            this.tab_page_gallons_4.Controls.Add(this.lbl_insert_quantity_gallons_4);
            this.tab_page_gallons_4.Controls.Add(this.txtbx_quantity_gallons_4);
            this.tab_page_gallons_4.Controls.Add(this.lbl_select_gasoline_4_1);
            this.tab_page_gallons_4.Controls.Add(this.cbx_gasoline_type_4_1);
            this.tab_page_gallons_4.Controls.Add(this.btn_bombear_galones_4);
            this.tab_page_gallons_4.Location = new System.Drawing.Point(4, 22);
            this.tab_page_gallons_4.Name = "tab_page_gallons_4";
            this.tab_page_gallons_4.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_gallons_4.Size = new System.Drawing.Size(243, 222);
            this.tab_page_gallons_4.TabIndex = 1;
            this.tab_page_gallons_4.Text = "Galónes";
            this.tab_page_gallons_4.UseVisualStyleBackColor = true;
            // 
            // btn_bombear_galones_4
            // 
            this.btn_bombear_galones_4.Depth = 0;
            this.btn_bombear_galones_4.Location = new System.Drawing.Point(112, 210);
            this.btn_bombear_galones_4.MouseState = MaterialSkin.MouseState.HOVER;
            this.btn_bombear_galones_4.Name = "btn_bombear_galones_4";
            this.btn_bombear_galones_4.Primary = true;
            this.btn_bombear_galones_4.Size = new System.Drawing.Size(128, 33);
            this.btn_bombear_galones_4.TabIndex = 6;
            this.btn_bombear_galones_4.Text = "BOMBEAR";
            this.btn_bombear_galones_4.UseVisualStyleBackColor = true;
            this.btn_bombear_galones_4.Click += new System.EventHandler(this.btn_bombear_galones_4_Click);
            // 
            // tab_page_info_4
            // 
            this.tab_page_info_4.Controls.Add(this.lbl_total_especial_b_4);
            this.tab_page_info_4.Controls.Add(this.lbl_total_regular_b_4);
            this.tab_page_info_4.Controls.Add(this.lbl_total_diesel_b_4);
            this.tab_page_info_4.Location = new System.Drawing.Point(4, 22);
            this.tab_page_info_4.Name = "tab_page_info_4";
            this.tab_page_info_4.Padding = new System.Windows.Forms.Padding(3);
            this.tab_page_info_4.Size = new System.Drawing.Size(243, 222);
            this.tab_page_info_4.TabIndex = 2;
            this.tab_page_info_4.Text = "Info";
            this.tab_page_info_4.UseVisualStyleBackColor = true;
            // 
            // pbx_bomb_4
            // 
            this.pbx_bomb_4.Image = ((System.Drawing.Image)(resources.GetObject("pbx_bomb_4.Image")));
            this.pbx_bomb_4.Location = new System.Drawing.Point(98, 36);
            this.pbx_bomb_4.Name = "pbx_bomb_4";
            this.pbx_bomb_4.Size = new System.Drawing.Size(60, 60);
            this.pbx_bomb_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbx_bomb_4.TabIndex = 0;
            this.pbx_bomb_4.TabStop = false;
            // 
            // lbl_total_diesel_b_1
            // 
            this.lbl_total_diesel_b_1.AutoSize = true;
            this.lbl_total_diesel_b_1.Location = new System.Drawing.Point(3, 27);
            this.lbl_total_diesel_b_1.Name = "lbl_total_diesel_b_1";
            this.lbl_total_diesel_b_1.Size = new System.Drawing.Size(74, 13);
            this.lbl_total_diesel_b_1.TabIndex = 0;
            this.lbl_total_diesel_b_1.Text = "Total diesel: X";
            // 
            // lbl_total_regular_b_1
            // 
            this.lbl_total_regular_b_1.AutoSize = true;
            this.lbl_total_regular_b_1.Location = new System.Drawing.Point(3, 48);
            this.lbl_total_regular_b_1.Name = "lbl_total_regular_b_1";
            this.lbl_total_regular_b_1.Size = new System.Drawing.Size(79, 13);
            this.lbl_total_regular_b_1.TabIndex = 1;
            this.lbl_total_regular_b_1.Text = "Total regular: X";
            // 
            // lbl_total_especial_b_1
            // 
            this.lbl_total_especial_b_1.AutoSize = true;
            this.lbl_total_especial_b_1.Location = new System.Drawing.Point(3, 70);
            this.lbl_total_especial_b_1.Name = "lbl_total_especial_b_1";
            this.lbl_total_especial_b_1.Size = new System.Drawing.Size(86, 13);
            this.lbl_total_especial_b_1.TabIndex = 2;
            this.lbl_total_especial_b_1.Text = "Total especial: X";
            // 
            // lbl_total_especial_b_2
            // 
            this.lbl_total_especial_b_2.AutoSize = true;
            this.lbl_total_especial_b_2.Location = new System.Drawing.Point(6, 70);
            this.lbl_total_especial_b_2.Name = "lbl_total_especial_b_2";
            this.lbl_total_especial_b_2.Size = new System.Drawing.Size(86, 13);
            this.lbl_total_especial_b_2.TabIndex = 5;
            this.lbl_total_especial_b_2.Text = "Total especial: X";
            // 
            // lbl_total_regular_b_2
            // 
            this.lbl_total_regular_b_2.AutoSize = true;
            this.lbl_total_regular_b_2.Location = new System.Drawing.Point(6, 48);
            this.lbl_total_regular_b_2.Name = "lbl_total_regular_b_2";
            this.lbl_total_regular_b_2.Size = new System.Drawing.Size(79, 13);
            this.lbl_total_regular_b_2.TabIndex = 4;
            this.lbl_total_regular_b_2.Text = "Total regular: X";
            // 
            // lbl_total_diesel_b_2
            // 
            this.lbl_total_diesel_b_2.AutoSize = true;
            this.lbl_total_diesel_b_2.Location = new System.Drawing.Point(6, 27);
            this.lbl_total_diesel_b_2.Name = "lbl_total_diesel_b_2";
            this.lbl_total_diesel_b_2.Size = new System.Drawing.Size(74, 13);
            this.lbl_total_diesel_b_2.TabIndex = 3;
            this.lbl_total_diesel_b_2.Text = "Total diesel: X";
            // 
            // lbl_total_especial_b_3
            // 
            this.lbl_total_especial_b_3.AutoSize = true;
            this.lbl_total_especial_b_3.Location = new System.Drawing.Point(6, 70);
            this.lbl_total_especial_b_3.Name = "lbl_total_especial_b_3";
            this.lbl_total_especial_b_3.Size = new System.Drawing.Size(86, 13);
            this.lbl_total_especial_b_3.TabIndex = 5;
            this.lbl_total_especial_b_3.Text = "Total especial: X";
            // 
            // lbl_total_regular_b_3
            // 
            this.lbl_total_regular_b_3.AutoSize = true;
            this.lbl_total_regular_b_3.Location = new System.Drawing.Point(6, 48);
            this.lbl_total_regular_b_3.Name = "lbl_total_regular_b_3";
            this.lbl_total_regular_b_3.Size = new System.Drawing.Size(79, 13);
            this.lbl_total_regular_b_3.TabIndex = 4;
            this.lbl_total_regular_b_3.Text = "Total regular: X";
            // 
            // lbl_total_diesel_b_3
            // 
            this.lbl_total_diesel_b_3.AutoSize = true;
            this.lbl_total_diesel_b_3.Location = new System.Drawing.Point(6, 27);
            this.lbl_total_diesel_b_3.Name = "lbl_total_diesel_b_3";
            this.lbl_total_diesel_b_3.Size = new System.Drawing.Size(74, 13);
            this.lbl_total_diesel_b_3.TabIndex = 3;
            this.lbl_total_diesel_b_3.Text = "Total diesel: X";
            // 
            // lbl_total_especial_b_4
            // 
            this.lbl_total_especial_b_4.AutoSize = true;
            this.lbl_total_especial_b_4.Location = new System.Drawing.Point(3, 70);
            this.lbl_total_especial_b_4.Name = "lbl_total_especial_b_4";
            this.lbl_total_especial_b_4.Size = new System.Drawing.Size(86, 13);
            this.lbl_total_especial_b_4.TabIndex = 8;
            this.lbl_total_especial_b_4.Text = "Total especial: X";
            // 
            // lbl_total_regular_b_4
            // 
            this.lbl_total_regular_b_4.AutoSize = true;
            this.lbl_total_regular_b_4.Location = new System.Drawing.Point(3, 48);
            this.lbl_total_regular_b_4.Name = "lbl_total_regular_b_4";
            this.lbl_total_regular_b_4.Size = new System.Drawing.Size(79, 13);
            this.lbl_total_regular_b_4.TabIndex = 7;
            this.lbl_total_regular_b_4.Text = "Total regular: X";
            // 
            // lbl_total_diesel_b_4
            // 
            this.lbl_total_diesel_b_4.AutoSize = true;
            this.lbl_total_diesel_b_4.Location = new System.Drawing.Point(3, 27);
            this.lbl_total_diesel_b_4.Name = "lbl_total_diesel_b_4";
            this.lbl_total_diesel_b_4.Size = new System.Drawing.Size(74, 13);
            this.lbl_total_diesel_b_4.TabIndex = 6;
            this.lbl_total_diesel_b_4.Text = "Total diesel: X";
            // 
            // lbl_total_especial_b_5
            // 
            this.lbl_total_especial_b_5.AutoSize = true;
            this.lbl_total_especial_b_5.Location = new System.Drawing.Point(6, 70);
            this.lbl_total_especial_b_5.Name = "lbl_total_especial_b_5";
            this.lbl_total_especial_b_5.Size = new System.Drawing.Size(86, 13);
            this.lbl_total_especial_b_5.TabIndex = 11;
            this.lbl_total_especial_b_5.Text = "Total especial: X";
            // 
            // lbl_total_regular_b_5
            // 
            this.lbl_total_regular_b_5.AutoSize = true;
            this.lbl_total_regular_b_5.Location = new System.Drawing.Point(6, 48);
            this.lbl_total_regular_b_5.Name = "lbl_total_regular_b_5";
            this.lbl_total_regular_b_5.Size = new System.Drawing.Size(79, 13);
            this.lbl_total_regular_b_5.TabIndex = 10;
            this.lbl_total_regular_b_5.Text = "Total regular: X";
            // 
            // lbl_total_diesel_b_5
            // 
            this.lbl_total_diesel_b_5.AutoSize = true;
            this.lbl_total_diesel_b_5.Location = new System.Drawing.Point(6, 27);
            this.lbl_total_diesel_b_5.Name = "lbl_total_diesel_b_5";
            this.lbl_total_diesel_b_5.Size = new System.Drawing.Size(74, 13);
            this.lbl_total_diesel_b_5.TabIndex = 9;
            this.lbl_total_diesel_b_5.Text = "Total diesel: X";
            // 
            // lbl_total_especial_b_6
            // 
            this.lbl_total_especial_b_6.AutoSize = true;
            this.lbl_total_especial_b_6.Location = new System.Drawing.Point(6, 70);
            this.lbl_total_especial_b_6.Name = "lbl_total_especial_b_6";
            this.lbl_total_especial_b_6.Size = new System.Drawing.Size(86, 13);
            this.lbl_total_especial_b_6.TabIndex = 14;
            this.lbl_total_especial_b_6.Text = "Total especial: X";
            // 
            // lbl_total_regular_b_6
            // 
            this.lbl_total_regular_b_6.AutoSize = true;
            this.lbl_total_regular_b_6.Location = new System.Drawing.Point(6, 48);
            this.lbl_total_regular_b_6.Name = "lbl_total_regular_b_6";
            this.lbl_total_regular_b_6.Size = new System.Drawing.Size(79, 13);
            this.lbl_total_regular_b_6.TabIndex = 13;
            this.lbl_total_regular_b_6.Text = "Total regular: X";
            // 
            // lbl_total_diesel_b_6
            // 
            this.lbl_total_diesel_b_6.AutoSize = true;
            this.lbl_total_diesel_b_6.Location = new System.Drawing.Point(6, 27);
            this.lbl_total_diesel_b_6.Name = "lbl_total_diesel_b_6";
            this.lbl_total_diesel_b_6.Size = new System.Drawing.Size(74, 13);
            this.lbl_total_diesel_b_6.TabIndex = 12;
            this.lbl_total_diesel_b_6.Text = "Total diesel: X";
            // 
            // txtbx_cantidad_money_1
            // 
            this.txtbx_cantidad_money_1.Location = new System.Drawing.Point(13, 98);
            this.txtbx_cantidad_money_1.Name = "txtbx_cantidad_money_1";
            this.txtbx_cantidad_money_1.Size = new System.Drawing.Size(199, 20);
            this.txtbx_cantidad_money_1.TabIndex = 3;
            this.txtbx_cantidad_money_1.Tag = "USE COMA.";
            // 
            // lbl_insert_quantity_money_1
            // 
            this.lbl_insert_quantity_money_1.AutoSize = true;
            this.lbl_insert_quantity_money_1.Depth = 0;
            this.lbl_insert_quantity_money_1.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_insert_quantity_money_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_money_1.Location = new System.Drawing.Point(12, 76);
            this.lbl_insert_quantity_money_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_money_1.Name = "lbl_insert_quantity_money_1";
            this.lbl_insert_quantity_money_1.Size = new System.Drawing.Size(96, 19);
            this.lbl_insert_quantity_money_1.TabIndex = 4;
            this.lbl_insert_quantity_money_1.Text = "Cantidad [$]: ";
            // 
            // lbl_insert_quantity_gallons_1
            // 
            this.lbl_insert_quantity_gallons_1.AutoSize = true;
            this.lbl_insert_quantity_gallons_1.Depth = 0;
            this.lbl_insert_quantity_gallons_1.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_insert_quantity_gallons_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_gallons_1.Location = new System.Drawing.Point(12, 76);
            this.lbl_insert_quantity_gallons_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_gallons_1.Name = "lbl_insert_quantity_gallons_1";
            this.lbl_insert_quantity_gallons_1.Size = new System.Drawing.Size(143, 19);
            this.lbl_insert_quantity_gallons_1.TabIndex = 12;
            this.lbl_insert_quantity_gallons_1.Text = "Cantidad [Galónes]: ";
            // 
            // txtbx_quantity_gallons_1
            // 
            this.txtbx_quantity_gallons_1.Location = new System.Drawing.Point(13, 98);
            this.txtbx_quantity_gallons_1.Name = "txtbx_quantity_gallons_1";
            this.txtbx_quantity_gallons_1.Size = new System.Drawing.Size(199, 20);
            this.txtbx_quantity_gallons_1.TabIndex = 11;
            // 
            // lbl_select_gasoline_1_1
            // 
            this.lbl_select_gasoline_1_1.AutoSize = true;
            this.lbl_select_gasoline_1_1.Depth = 0;
            this.lbl_select_gasoline_1_1.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_select_gasoline_1_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_select_gasoline_1_1.Location = new System.Drawing.Point(12, 21);
            this.lbl_select_gasoline_1_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_select_gasoline_1_1.Name = "lbl_select_gasoline_1_1";
            this.lbl_select_gasoline_1_1.Size = new System.Drawing.Size(104, 19);
            this.lbl_select_gasoline_1_1.TabIndex = 10;
            this.lbl_select_gasoline_1_1.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_1_1
            // 
            this.cbx_gasoline_type_1_1.FormattingEnabled = true;
            this.cbx_gasoline_type_1_1.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_1_1.Location = new System.Drawing.Point(13, 43);
            this.cbx_gasoline_type_1_1.Name = "cbx_gasoline_type_1_1";
            this.cbx_gasoline_type_1_1.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_1_1.TabIndex = 9;
            // 
            // lbl_insert_quantity_money_2
            // 
            this.lbl_insert_quantity_money_2.AutoSize = true;
            this.lbl_insert_quantity_money_2.Depth = 0;
            this.lbl_insert_quantity_money_2.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_insert_quantity_money_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_money_2.Location = new System.Drawing.Point(15, 76);
            this.lbl_insert_quantity_money_2.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_money_2.Name = "lbl_insert_quantity_money_2";
            this.lbl_insert_quantity_money_2.Size = new System.Drawing.Size(96, 19);
            this.lbl_insert_quantity_money_2.TabIndex = 10;
            this.lbl_insert_quantity_money_2.Text = "Cantidad [$]: ";
            // 
            // txtbx_cantidad_money_2
            // 
            this.txtbx_cantidad_money_2.Location = new System.Drawing.Point(16, 98);
            this.txtbx_cantidad_money_2.Name = "txtbx_cantidad_money_2";
            this.txtbx_cantidad_money_2.Size = new System.Drawing.Size(199, 20);
            this.txtbx_cantidad_money_2.TabIndex = 9;
            this.txtbx_cantidad_money_2.Tag = "USE COMA";
            // 
            // lbl_insert_quantity_gallons_2
            // 
            this.lbl_insert_quantity_gallons_2.AutoSize = true;
            this.lbl_insert_quantity_gallons_2.Depth = 0;
            this.lbl_insert_quantity_gallons_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lbl_insert_quantity_gallons_2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_gallons_2.Location = new System.Drawing.Point(15, 76);
            this.lbl_insert_quantity_gallons_2.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_gallons_2.Name = "lbl_insert_quantity_gallons_2";
            this.lbl_insert_quantity_gallons_2.Size = new System.Drawing.Size(142, 18);
            this.lbl_insert_quantity_gallons_2.TabIndex = 14;
            this.lbl_insert_quantity_gallons_2.Text = "Cantidad [Galónes]: ";
            // 
            // txtbx_quantity_gallons_2
            // 
            this.txtbx_quantity_gallons_2.Location = new System.Drawing.Point(16, 98);
            this.txtbx_quantity_gallons_2.Name = "txtbx_quantity_gallons_2";
            this.txtbx_quantity_gallons_2.Size = new System.Drawing.Size(199, 20);
            this.txtbx_quantity_gallons_2.TabIndex = 13;
            // 
            // lbl_insert_quantity_gallons_3
            // 
            this.lbl_insert_quantity_gallons_3.AutoSize = true;
            this.lbl_insert_quantity_gallons_3.Depth = 0;
            this.lbl_insert_quantity_gallons_3.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_insert_quantity_gallons_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_gallons_3.Location = new System.Drawing.Point(15, 76);
            this.lbl_insert_quantity_gallons_3.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_gallons_3.Name = "lbl_insert_quantity_gallons_3";
            this.lbl_insert_quantity_gallons_3.Size = new System.Drawing.Size(143, 19);
            this.lbl_insert_quantity_gallons_3.TabIndex = 16;
            this.lbl_insert_quantity_gallons_3.Text = "Cantidad [Galónes]: ";
            // 
            // txtbx_quantity_gallons_3
            // 
            this.txtbx_quantity_gallons_3.Location = new System.Drawing.Point(16, 98);
            this.txtbx_quantity_gallons_3.Name = "txtbx_quantity_gallons_3";
            this.txtbx_quantity_gallons_3.Size = new System.Drawing.Size(199, 20);
            this.txtbx_quantity_gallons_3.TabIndex = 15;
            // 
            // lbl_insert_quantity_money_3
            // 
            this.lbl_insert_quantity_money_3.AutoSize = true;
            this.lbl_insert_quantity_money_3.Depth = 0;
            this.lbl_insert_quantity_money_3.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_insert_quantity_money_3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_money_3.Location = new System.Drawing.Point(12, 76);
            this.lbl_insert_quantity_money_3.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_money_3.Name = "lbl_insert_quantity_money_3";
            this.lbl_insert_quantity_money_3.Size = new System.Drawing.Size(96, 19);
            this.lbl_insert_quantity_money_3.TabIndex = 12;
            this.lbl_insert_quantity_money_3.Text = "Cantidad [$]: ";
            // 
            // txtbx_cantidad_money_3
            // 
            this.txtbx_cantidad_money_3.Location = new System.Drawing.Point(13, 98);
            this.txtbx_cantidad_money_3.Name = "txtbx_cantidad_money_3";
            this.txtbx_cantidad_money_3.Size = new System.Drawing.Size(199, 20);
            this.txtbx_cantidad_money_3.TabIndex = 11;
            // 
            // lbl_insert_quantity_gallons_4
            // 
            this.lbl_insert_quantity_gallons_4.AutoSize = true;
            this.lbl_insert_quantity_gallons_4.Depth = 0;
            this.lbl_insert_quantity_gallons_4.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_insert_quantity_gallons_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_gallons_4.Location = new System.Drawing.Point(12, 76);
            this.lbl_insert_quantity_gallons_4.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_gallons_4.Name = "lbl_insert_quantity_gallons_4";
            this.lbl_insert_quantity_gallons_4.Size = new System.Drawing.Size(143, 19);
            this.lbl_insert_quantity_gallons_4.TabIndex = 16;
            this.lbl_insert_quantity_gallons_4.Text = "Cantidad [Galónes]: ";
            // 
            // txtbx_quantity_gallons_4
            // 
            this.txtbx_quantity_gallons_4.Location = new System.Drawing.Point(13, 98);
            this.txtbx_quantity_gallons_4.Name = "txtbx_quantity_gallons_4";
            this.txtbx_quantity_gallons_4.Size = new System.Drawing.Size(199, 20);
            this.txtbx_quantity_gallons_4.TabIndex = 15;
            // 
            // lbl_select_gasoline_4_1
            // 
            this.lbl_select_gasoline_4_1.AutoSize = true;
            this.lbl_select_gasoline_4_1.Depth = 0;
            this.lbl_select_gasoline_4_1.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_select_gasoline_4_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_select_gasoline_4_1.Location = new System.Drawing.Point(12, 21);
            this.lbl_select_gasoline_4_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_select_gasoline_4_1.Name = "lbl_select_gasoline_4_1";
            this.lbl_select_gasoline_4_1.Size = new System.Drawing.Size(104, 19);
            this.lbl_select_gasoline_4_1.TabIndex = 14;
            this.lbl_select_gasoline_4_1.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_4_1
            // 
            this.cbx_gasoline_type_4_1.FormattingEnabled = true;
            this.cbx_gasoline_type_4_1.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_4_1.Location = new System.Drawing.Point(13, 43);
            this.cbx_gasoline_type_4_1.Name = "cbx_gasoline_type_4_1";
            this.cbx_gasoline_type_4_1.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_4_1.TabIndex = 13;
            // 
            // lbl_insert_quantity_gallons_5
            // 
            this.lbl_insert_quantity_gallons_5.AutoSize = true;
            this.lbl_insert_quantity_gallons_5.Depth = 0;
            this.lbl_insert_quantity_gallons_5.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_insert_quantity_gallons_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_gallons_5.Location = new System.Drawing.Point(10, 76);
            this.lbl_insert_quantity_gallons_5.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_gallons_5.Name = "lbl_insert_quantity_gallons_5";
            this.lbl_insert_quantity_gallons_5.Size = new System.Drawing.Size(143, 19);
            this.lbl_insert_quantity_gallons_5.TabIndex = 20;
            this.lbl_insert_quantity_gallons_5.Text = "Cantidad [Galónes]: ";
            // 
            // txtbx_quantity_gallons_5
            // 
            this.txtbx_quantity_gallons_5.Location = new System.Drawing.Point(11, 98);
            this.txtbx_quantity_gallons_5.Name = "txtbx_quantity_gallons_5";
            this.txtbx_quantity_gallons_5.Size = new System.Drawing.Size(199, 20);
            this.txtbx_quantity_gallons_5.TabIndex = 19;
            // 
            // lbl_select_gasoline_5_1
            // 
            this.lbl_select_gasoline_5_1.AutoSize = true;
            this.lbl_select_gasoline_5_1.Depth = 0;
            this.lbl_select_gasoline_5_1.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_select_gasoline_5_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_select_gasoline_5_1.Location = new System.Drawing.Point(10, 21);
            this.lbl_select_gasoline_5_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_select_gasoline_5_1.Name = "lbl_select_gasoline_5_1";
            this.lbl_select_gasoline_5_1.Size = new System.Drawing.Size(104, 19);
            this.lbl_select_gasoline_5_1.TabIndex = 18;
            this.lbl_select_gasoline_5_1.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_5_1
            // 
            this.cbx_gasoline_type_5_1.FormattingEnabled = true;
            this.cbx_gasoline_type_5_1.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_5_1.Location = new System.Drawing.Point(11, 43);
            this.cbx_gasoline_type_5_1.Name = "cbx_gasoline_type_5_1";
            this.cbx_gasoline_type_5_1.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_5_1.TabIndex = 17;
            // 
            // lbl_insert_quantity_gallons_6
            // 
            this.lbl_insert_quantity_gallons_6.AutoSize = true;
            this.lbl_insert_quantity_gallons_6.Depth = 0;
            this.lbl_insert_quantity_gallons_6.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_insert_quantity_gallons_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_gallons_6.Location = new System.Drawing.Point(9, 76);
            this.lbl_insert_quantity_gallons_6.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_gallons_6.Name = "lbl_insert_quantity_gallons_6";
            this.lbl_insert_quantity_gallons_6.Size = new System.Drawing.Size(143, 19);
            this.lbl_insert_quantity_gallons_6.TabIndex = 24;
            this.lbl_insert_quantity_gallons_6.Text = "Cantidad [Galónes]: ";
            // 
            // txtbx_quantity_gallons_6
            // 
            this.txtbx_quantity_gallons_6.Location = new System.Drawing.Point(10, 98);
            this.txtbx_quantity_gallons_6.Name = "txtbx_quantity_gallons_6";
            this.txtbx_quantity_gallons_6.Size = new System.Drawing.Size(199, 20);
            this.txtbx_quantity_gallons_6.TabIndex = 23;
            // 
            // lbl_select_gasoline_6_1
            // 
            this.lbl_select_gasoline_6_1.AutoSize = true;
            this.lbl_select_gasoline_6_1.Depth = 0;
            this.lbl_select_gasoline_6_1.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_select_gasoline_6_1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_select_gasoline_6_1.Location = new System.Drawing.Point(9, 21);
            this.lbl_select_gasoline_6_1.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_select_gasoline_6_1.Name = "lbl_select_gasoline_6_1";
            this.lbl_select_gasoline_6_1.Size = new System.Drawing.Size(104, 19);
            this.lbl_select_gasoline_6_1.TabIndex = 22;
            this.lbl_select_gasoline_6_1.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_6_1
            // 
            this.cbx_gasoline_type_6_1.FormattingEnabled = true;
            this.cbx_gasoline_type_6_1.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_6_1.Location = new System.Drawing.Point(10, 43);
            this.cbx_gasoline_type_6_1.Name = "cbx_gasoline_type_6_1";
            this.cbx_gasoline_type_6_1.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_6_1.TabIndex = 21;
            // 
            // lbl_insert_quantity_money_4
            // 
            this.lbl_insert_quantity_money_4.AutoSize = true;
            this.lbl_insert_quantity_money_4.Depth = 0;
            this.lbl_insert_quantity_money_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lbl_insert_quantity_money_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_money_4.Location = new System.Drawing.Point(12, 76);
            this.lbl_insert_quantity_money_4.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_money_4.Name = "lbl_insert_quantity_money_4";
            this.lbl_insert_quantity_money_4.Size = new System.Drawing.Size(94, 18);
            this.lbl_insert_quantity_money_4.TabIndex = 16;
            this.lbl_insert_quantity_money_4.Text = "Cantidad [$]: ";
            // 
            // txtbx_cantidad_money_4
            // 
            this.txtbx_cantidad_money_4.Location = new System.Drawing.Point(13, 98);
            this.txtbx_cantidad_money_4.Name = "txtbx_cantidad_money_4";
            this.txtbx_cantidad_money_4.Size = new System.Drawing.Size(199, 20);
            this.txtbx_cantidad_money_4.TabIndex = 15;
            // 
            // lbl_select_gasoline_4
            // 
            this.lbl_select_gasoline_4.AutoSize = true;
            this.lbl_select_gasoline_4.Depth = 0;
            this.lbl_select_gasoline_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lbl_select_gasoline_4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_select_gasoline_4.Location = new System.Drawing.Point(12, 21);
            this.lbl_select_gasoline_4.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_select_gasoline_4.Name = "lbl_select_gasoline_4";
            this.lbl_select_gasoline_4.Size = new System.Drawing.Size(100, 18);
            this.lbl_select_gasoline_4.TabIndex = 14;
            this.lbl_select_gasoline_4.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_4
            // 
            this.cbx_gasoline_type_4.FormattingEnabled = true;
            this.cbx_gasoline_type_4.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_4.Location = new System.Drawing.Point(13, 43);
            this.cbx_gasoline_type_4.Name = "cbx_gasoline_type_4";
            this.cbx_gasoline_type_4.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_4.TabIndex = 13;
            // 
            // lbl_insert_quantity_money_5
            // 
            this.lbl_insert_quantity_money_5.AutoSize = true;
            this.lbl_insert_quantity_money_5.Depth = 0;
            this.lbl_insert_quantity_money_5.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_insert_quantity_money_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_money_5.Location = new System.Drawing.Point(6, 76);
            this.lbl_insert_quantity_money_5.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_money_5.Name = "lbl_insert_quantity_money_5";
            this.lbl_insert_quantity_money_5.Size = new System.Drawing.Size(96, 19);
            this.lbl_insert_quantity_money_5.TabIndex = 20;
            this.lbl_insert_quantity_money_5.Text = "Cantidad [$]: ";
            // 
            // txtbx_cantidad_money_5
            // 
            this.txtbx_cantidad_money_5.Location = new System.Drawing.Point(7, 98);
            this.txtbx_cantidad_money_5.Name = "txtbx_cantidad_money_5";
            this.txtbx_cantidad_money_5.Size = new System.Drawing.Size(199, 20);
            this.txtbx_cantidad_money_5.TabIndex = 19;
            // 
            // lbl_select_gasoline_5
            // 
            this.lbl_select_gasoline_5.AutoSize = true;
            this.lbl_select_gasoline_5.Depth = 0;
            this.lbl_select_gasoline_5.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_select_gasoline_5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_select_gasoline_5.Location = new System.Drawing.Point(6, 21);
            this.lbl_select_gasoline_5.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_select_gasoline_5.Name = "lbl_select_gasoline_5";
            this.lbl_select_gasoline_5.Size = new System.Drawing.Size(104, 19);
            this.lbl_select_gasoline_5.TabIndex = 18;
            this.lbl_select_gasoline_5.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_5
            // 
            this.cbx_gasoline_type_5.FormattingEnabled = true;
            this.cbx_gasoline_type_5.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_5.Location = new System.Drawing.Point(7, 43);
            this.cbx_gasoline_type_5.Name = "cbx_gasoline_type_5";
            this.cbx_gasoline_type_5.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_5.TabIndex = 17;
            // 
            // lbl_insert_quantity_money_6
            // 
            this.lbl_insert_quantity_money_6.AutoSize = true;
            this.lbl_insert_quantity_money_6.Depth = 0;
            this.lbl_insert_quantity_money_6.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_insert_quantity_money_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_insert_quantity_money_6.Location = new System.Drawing.Point(5, 76);
            this.lbl_insert_quantity_money_6.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_insert_quantity_money_6.Name = "lbl_insert_quantity_money_6";
            this.lbl_insert_quantity_money_6.Size = new System.Drawing.Size(96, 19);
            this.lbl_insert_quantity_money_6.TabIndex = 24;
            this.lbl_insert_quantity_money_6.Text = "Cantidad [$]: ";
            // 
            // txtbx_cantidad_money_6
            // 
            this.txtbx_cantidad_money_6.Location = new System.Drawing.Point(6, 98);
            this.txtbx_cantidad_money_6.Name = "txtbx_cantidad_money_6";
            this.txtbx_cantidad_money_6.Size = new System.Drawing.Size(199, 20);
            this.txtbx_cantidad_money_6.TabIndex = 23;
            // 
            // lbl_select_gasoline_6
            // 
            this.lbl_select_gasoline_6.AutoSize = true;
            this.lbl_select_gasoline_6.Depth = 0;
            this.lbl_select_gasoline_6.Font = new System.Drawing.Font("Roboto", 11F);
            this.lbl_select_gasoline_6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_select_gasoline_6.Location = new System.Drawing.Point(5, 21);
            this.lbl_select_gasoline_6.MouseState = MaterialSkin.MouseState.HOVER;
            this.lbl_select_gasoline_6.Name = "lbl_select_gasoline_6";
            this.lbl_select_gasoline_6.Size = new System.Drawing.Size(104, 19);
            this.lbl_select_gasoline_6.TabIndex = 22;
            this.lbl_select_gasoline_6.Text = "Tipo gasolina:";
            // 
            // cbx_gasoline_type_6
            // 
            this.cbx_gasoline_type_6.FormattingEnabled = true;
            this.cbx_gasoline_type_6.Items.AddRange(new object[] {
            "Diesel - $4.25",
            "Regular - $3.95",
            "Especial - $4.25"});
            this.cbx_gasoline_type_6.Location = new System.Drawing.Point(6, 43);
            this.cbx_gasoline_type_6.Name = "cbx_gasoline_type_6";
            this.cbx_gasoline_type_6.Size = new System.Drawing.Size(199, 21);
            this.cbx_gasoline_type_6.TabIndex = 21;
            // 
            // FrmMainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 620);
            this.Controls.Add(this.tab_container_bombs);
            this.Controls.Add(this.tab_selector_bombs);
            this.Name = "FrmMainView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GAS STATION";
            this.Load += new System.EventHandler(this.FrmMainView_Load);
            this.tab_container_bombs.ResumeLayout(false);
            this.tab_page_bombs_1_3.ResumeLayout(false);
            this.gbx_bomb_3.ResumeLayout(false);
            this.tab_control_bomb_3.ResumeLayout(false);
            this.tab_page_money_3.ResumeLayout(false);
            this.tab_page_money_3.PerformLayout();
            this.tab_page_gallons_3.ResumeLayout(false);
            this.tab_page_gallons_3.PerformLayout();
            this.tab_page_info_3.ResumeLayout(false);
            this.tab_page_info_3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_3)).EndInit();
            this.gbx_bomb_2.ResumeLayout(false);
            this.tab_control_bomb_2.ResumeLayout(false);
            this.tab_page_bomb_2.ResumeLayout(false);
            this.tab_page_bomb_2.PerformLayout();
            this.tab_page_gallons_2.ResumeLayout(false);
            this.tab_page_gallons_2.PerformLayout();
            this.tab_page_info_2.ResumeLayout(false);
            this.tab_page_info_2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_2)).EndInit();
            this.gbx_bomb_1.ResumeLayout(false);
            this.tab_control_bomb_1.ResumeLayout(false);
            this.tab_page_money_1.ResumeLayout(false);
            this.tab_page_money_1.PerformLayout();
            this.tab_page_gallons_1.ResumeLayout(false);
            this.tab_page_gallons_1.PerformLayout();
            this.tab_page_info_1.ResumeLayout(false);
            this.tab_page_info_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_1)).EndInit();
            this.tab_page_bombs_4_6.ResumeLayout(false);
            this.gbx_bomb_6.ResumeLayout(false);
            this.tab_control_bomb_6.ResumeLayout(false);
            this.tab_page_money_6.ResumeLayout(false);
            this.tab_page_money_6.PerformLayout();
            this.tab_page_gallons_6.ResumeLayout(false);
            this.tab_page_gallons_6.PerformLayout();
            this.tab_page_info_6.ResumeLayout(false);
            this.tab_page_info_6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_6)).EndInit();
            this.gbx_bomb_5.ResumeLayout(false);
            this.tab_control_bomb_5.ResumeLayout(false);
            this.tab_page_money_5.ResumeLayout(false);
            this.tab_page_money_5.PerformLayout();
            this.tab_page_gallons_5.ResumeLayout(false);
            this.tab_page_gallons_5.PerformLayout();
            this.tab_page_info_5.ResumeLayout(false);
            this.tab_page_info_5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_5)).EndInit();
            this.gbx_bomb_4.ResumeLayout(false);
            this.tab_control_bomb_4.ResumeLayout(false);
            this.tab_page_money_4.ResumeLayout(false);
            this.tab_page_money_4.PerformLayout();
            this.tab_page_gallons_4.ResumeLayout(false);
            this.tab_page_gallons_4.PerformLayout();
            this.tab_page_info_4.ResumeLayout(false);
            this.tab_page_info_4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbx_bomb_4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabSelector tab_selector_bombs;
        private MaterialSkin.Controls.MaterialTabControl tab_container_bombs;
        private System.Windows.Forms.TabPage tab_page_bombs_1_3;
        private System.Windows.Forms.GroupBox gbx_bomb_3;
        private MaterialSkin.Controls.MaterialTabSelector tab_selector_bomb_3;
        private MaterialSkin.Controls.MaterialTabControl tab_control_bomb_3;
        private System.Windows.Forms.TabPage tab_page_money_3;
        private System.Windows.Forms.TabPage tab_page_gallons_3;
        private System.Windows.Forms.PictureBox pbx_bomb_3;
        private System.Windows.Forms.GroupBox gbx_bomb_2;
        private MaterialSkin.Controls.MaterialTabSelector tab_selector_bomb_2;
        private MaterialSkin.Controls.MaterialTabControl tab_control_bomb_2;
        private System.Windows.Forms.TabPage tab_page_bomb_2;
        private System.Windows.Forms.TabPage tab_page_gallons_2;
        private System.Windows.Forms.PictureBox pbx_bomb_2;
        private System.Windows.Forms.GroupBox gbx_bomb_1;
        private MaterialSkin.Controls.MaterialTabSelector tab_selector_bomb_1;
        private MaterialSkin.Controls.MaterialTabControl tab_control_bomb_1;
        private System.Windows.Forms.TabPage tab_page_money_1;
        private System.Windows.Forms.TabPage tab_page_gallons_1;
        private System.Windows.Forms.PictureBox pbx_bomb_1;
        private System.Windows.Forms.TabPage tab_page_bombs_4_6;
        private System.Windows.Forms.GroupBox gbx_bomb_6;
        private MaterialSkin.Controls.MaterialTabSelector tab_selector_bomb_6;
        private MaterialSkin.Controls.MaterialTabControl tab_control_bomb_6;
        private System.Windows.Forms.TabPage tab_page_money_6;
        private System.Windows.Forms.TabPage tab_page_gallons_6;
        private System.Windows.Forms.PictureBox pbx_bomb_6;
        private System.Windows.Forms.GroupBox gbx_bomb_5;
        private MaterialSkin.Controls.MaterialTabSelector tab_selector_bomb_5;
        private MaterialSkin.Controls.MaterialTabControl tab_control_bomb_5;
        private System.Windows.Forms.TabPage tab_page_money_5;
        private System.Windows.Forms.TabPage tab_page_gallons_5;
        private System.Windows.Forms.PictureBox pbx_bomb_5;
        private System.Windows.Forms.GroupBox gbx_bomb_4;
        private MaterialSkin.Controls.MaterialTabSelector tab_selector_bomb_4;
        private MaterialSkin.Controls.MaterialTabControl tab_control_bomb_4;
        private System.Windows.Forms.TabPage tab_page_money_4;
        private System.Windows.Forms.TabPage tab_page_gallons_4;
        private System.Windows.Forms.PictureBox pbx_bomb_4;
        private System.Windows.Forms.TabPage tab_page_info_3;
        private System.Windows.Forms.TabPage tab_page_info_2;
        private System.Windows.Forms.TabPage tab_page_info_1;
        private System.Windows.Forms.TabPage tab_page_info_6;
        private System.Windows.Forms.TabPage tab_page_info_5;
        private System.Windows.Forms.TabPage tab_page_info_4;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bomb_money_2;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bombear_galones_2;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bomb_money_1;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bombear_galones_1;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bomb_money_3;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bombear_galones_3;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bomb_money_6;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bomb_money_4;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bomb_money_5;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bombear_galones_5;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bombear_galones_4;
        private MaterialSkin.Controls.MaterialLabel lbl_gasoline_type_3;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_3;
        private MaterialSkin.Controls.MaterialLabel lbl_gasoline_type_3_1;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_3_1;
        private MaterialSkin.Controls.MaterialLabel lbl_gasoline_type_2;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_2;
        private MaterialSkin.Controls.MaterialLabel lbl_gasoline_type_2_1;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_2_1;
        private MaterialSkin.Controls.MaterialLabel lbl_select_gasoline_1;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_1;
        private MaterialSkin.Controls.MaterialRaisedButton btn_bombear_galones_6;
        private System.Windows.Forms.Label lbl_total_especial_b_1;
        private System.Windows.Forms.Label lbl_total_regular_b_1;
        private System.Windows.Forms.Label lbl_total_diesel_b_1;
        private System.Windows.Forms.Label lbl_total_especial_b_3;
        private System.Windows.Forms.Label lbl_total_regular_b_3;
        private System.Windows.Forms.Label lbl_total_diesel_b_3;
        private System.Windows.Forms.Label lbl_total_especial_b_2;
        private System.Windows.Forms.Label lbl_total_regular_b_2;
        private System.Windows.Forms.Label lbl_total_diesel_b_2;
        private System.Windows.Forms.Label lbl_total_especial_b_6;
        private System.Windows.Forms.Label lbl_total_diesel_b_6;
        private System.Windows.Forms.Label lbl_total_regular_b_6;
        private System.Windows.Forms.Label lbl_total_especial_b_5;
        private System.Windows.Forms.Label lbl_total_regular_b_5;
        private System.Windows.Forms.Label lbl_total_diesel_b_5;
        private System.Windows.Forms.Label lbl_total_especial_b_4;
        private System.Windows.Forms.Label lbl_total_regular_b_4;
        private System.Windows.Forms.Label lbl_total_diesel_b_4;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_money_1;
        private System.Windows.Forms.TextBox txtbx_cantidad_money_1;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_gallons_1;
        private System.Windows.Forms.TextBox txtbx_quantity_gallons_1;
        private MaterialSkin.Controls.MaterialLabel lbl_select_gasoline_1_1;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_1_1;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_gallons_3;
        private System.Windows.Forms.TextBox txtbx_quantity_gallons_3;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_money_2;
        private System.Windows.Forms.TextBox txtbx_cantidad_money_2;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_gallons_2;
        private System.Windows.Forms.TextBox txtbx_quantity_gallons_2;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_money_3;
        private System.Windows.Forms.TextBox txtbx_cantidad_money_3;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_money_6;
        private System.Windows.Forms.TextBox txtbx_cantidad_money_6;
        private MaterialSkin.Controls.MaterialLabel lbl_select_gasoline_6;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_6;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_gallons_6;
        private System.Windows.Forms.TextBox txtbx_quantity_gallons_6;
        private MaterialSkin.Controls.MaterialLabel lbl_select_gasoline_6_1;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_6_1;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_money_5;
        private System.Windows.Forms.TextBox txtbx_cantidad_money_5;
        private MaterialSkin.Controls.MaterialLabel lbl_select_gasoline_5;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_5;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_gallons_5;
        private System.Windows.Forms.TextBox txtbx_quantity_gallons_5;
        private MaterialSkin.Controls.MaterialLabel lbl_select_gasoline_5_1;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_5_1;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_money_4;
        private System.Windows.Forms.TextBox txtbx_cantidad_money_4;
        private MaterialSkin.Controls.MaterialLabel lbl_select_gasoline_4;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_4;
        private MaterialSkin.Controls.MaterialLabel lbl_insert_quantity_gallons_4;
        private System.Windows.Forms.TextBox txtbx_quantity_gallons_4;
        private MaterialSkin.Controls.MaterialLabel lbl_select_gasoline_4_1;
        private System.Windows.Forms.ComboBox cbx_gasoline_type_4_1;
    }
}