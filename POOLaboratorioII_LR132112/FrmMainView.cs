﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POOLaboratorioII_LR132112
{
    public partial class FrmMainView : MaterialSkin.Controls.MaterialForm
    {
        classes._Util utils = new classes._Util();

        List<classes._Bomb> bomb = new classes._Util().bombsList();

        public FrmMainView()
        {
            InitializeComponent();
            new classes._Util().setMaterialSkin(this);   
        }

        private void FrmMainView_Load(object sender, EventArgs e)
        {
            setBombsInformation();
        }

        private void tab_gallons_1_Click(object sender, EventArgs e){}
        private void tab_page_info_1_Click(object sender, EventArgs e){}
        private void tab_page_info_2_Click(object sender, EventArgs e){}
        private void tab_page_money_3_Click(object sender, EventArgs e){}
        private void tab_page_gallons_5_Click(object sender, EventArgs e) { }
        private void tab_page_gallons_3_Click(object sender, EventArgs e) { }

        private void groupBox2_Enter(object sender, EventArgs e) { }





        private void setBombsInformation() 
        {
            for(int j = 1; j <= bomb.Count; j++)
            {
                this.Controls.Find(string.Format("lbl_total_diesel_b_{0}", j), true).FirstOrDefault().Text = "Total diesel B" + j + ":" + bomb[j-1].Diesel_gasoline.ToString() + " Galones";
                this.Controls.Find(string.Format("lbl_total_regular_b_{0}", j), true).FirstOrDefault().Text = "Total regular B" + j + ":" + bomb[j-1].Regular_gasoline.ToString() + " Galones";
                this.Controls.Find(string.Format("lbl_total_especial_b_{0}", j), true).FirstOrDefault().Text = "Total especial B"+j+":" + bomb[j-1].Special_gasoline.ToString() + " Galones";
            }
            
        }





        /** BOMBA #1 **/

        private void btn_bomb_money_1_Click(object sender, EventArgs e)
        {
            if (!validateFields(1, 0))
            {
                setNewBombValue(
                utils.discoutnWithMoney(
                    1,
                    getTotalBomb(cbx_gasoline_type_1.SelectedIndex, 1),
                    getPriceBomb(cbx_gasoline_type_1.SelectedIndex, 1),
                    Convert.ToDouble(txtbx_cantidad_money_1.Text)),
                cbx_gasoline_type_1.SelectedIndex,
                1);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }

        private void btn_bombear_galones_1_Click(object sender, EventArgs e)
        {
            if (!validateFields(1, 1))
            {
                setNewBombValue(
                utils.discountWithGallons(
                    1,
                    getTotalBomb(cbx_gasoline_type_1_1.SelectedIndex, 1),
                    getPriceBomb(cbx_gasoline_type_1_1.SelectedIndex, 1),
                    Convert.ToInt32(txtbx_quantity_gallons_1.Text)),
                cbx_gasoline_type_1_1.SelectedIndex,
                1);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }





        /** BOMBA #2 **/

        private void btn_bomb_money_2_Click(object sender, EventArgs e)
        {
            if (!validateFields(2, 0))
            {
                setNewBombValue(
                utils.discoutnWithMoney(
                    2,
                    getTotalBomb(cbx_gasoline_type_2.SelectedIndex, 2),
                    getPriceBomb(cbx_gasoline_type_2.SelectedIndex, 2),
                    Convert.ToDouble(txtbx_cantidad_money_2.Text)),
                cbx_gasoline_type_2.SelectedIndex,
                2);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }

        private void btn_bombear_galones_2_Click(object sender, EventArgs e)
        {
            if (!validateFields(2, 1))
            {
                setNewBombValue(
                   utils.discountWithGallons(
                       2,
                       getTotalBomb(cbx_gasoline_type_2_1.SelectedIndex, 2),
                       getPriceBomb(cbx_gasoline_type_2_1.SelectedIndex, 2),
                       Convert.ToInt32(txtbx_quantity_gallons_2.Text)),
                   cbx_gasoline_type_2_1.SelectedIndex,
                   2);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }





        /** BOMBA #3 **/

        private void btn_bomb_money_3_Click(object sender, EventArgs e)
        {
            if (!validateFields(3, 0))
            {
                setNewBombValue(
                    utils.discoutnWithMoney(
                        3,
                        getTotalBomb(cbx_gasoline_type_3.SelectedIndex, 3),
                        getPriceBomb(cbx_gasoline_type_3.SelectedIndex, 3),
                        Convert.ToDouble(txtbx_cantidad_money_3.Text)),
                    cbx_gasoline_type_3.SelectedIndex,
                    3);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }

        private void btn_bombear_galones_3_Click(object sender, EventArgs e)
        {
            if (!validateFields(3, 1))
            {
                setNewBombValue(
                   utils.discountWithGallons(
                       3,
                       getTotalBomb(cbx_gasoline_type_3_1.SelectedIndex, 3),
                       getPriceBomb(cbx_gasoline_type_3_1.SelectedIndex, 3),
                       Convert.ToInt32(txtbx_quantity_gallons_3.Text)),
                   cbx_gasoline_type_3_1.SelectedIndex,
                   3);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }





        /** BOMBA #4 **/

        private void btn_bomb_money_4_Click(object sender, EventArgs e)
        {
            if (!validateFields(4, 0))
            {
                setNewBombValue(
                    utils.discoutnWithMoney(
                        4,
                        getTotalBomb(cbx_gasoline_type_4.SelectedIndex, 4),
                        getPriceBomb(cbx_gasoline_type_4.SelectedIndex, 4),
                        Convert.ToDouble(txtbx_cantidad_money_4.Text)),
                    cbx_gasoline_type_4.SelectedIndex,
                    4);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }

        private void btn_bombear_galones_4_Click(object sender, EventArgs e)
        {
            if (!validateFields(4, 1))
            {
                setNewBombValue(
                   utils.discountWithGallons(
                       4,
                       getTotalBomb(cbx_gasoline_type_4_1.SelectedIndex, 4),
                       getPriceBomb(cbx_gasoline_type_4_1.SelectedIndex, 4),
                       Convert.ToInt32(txtbx_quantity_gallons_4.Text)),
                   cbx_gasoline_type_4_1.SelectedIndex,
                   4);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }





        /** BOMBA #5 **/

        private void btn_bomb_money_5_Click(object sender, EventArgs e)
        {
            if (!validateFields(5, 0))
            {
                setNewBombValue(
                    utils.discoutnWithMoney(
                        5,
                        getTotalBomb(cbx_gasoline_type_5.SelectedIndex, 5),
                        getPriceBomb(cbx_gasoline_type_5.SelectedIndex, 5),
                        Convert.ToDouble(txtbx_cantidad_money_5.Text)),
                    cbx_gasoline_type_5.SelectedIndex,
                    5);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }

        private void btn_bombear_galones_5_Click(object sender, EventArgs e)
        {
            if (!validateFields(5, 1))
            {
                setNewBombValue(
                   utils.discountWithGallons(
                       5,
                       getTotalBomb(cbx_gasoline_type_5_1.SelectedIndex, 5),
                       getPriceBomb(cbx_gasoline_type_5_1.SelectedIndex, 5),
                       Convert.ToInt32(txtbx_quantity_gallons_5.Text)),
                   cbx_gasoline_type_5_1.SelectedIndex,
                   5);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }

        }





        /** BOMBA #6 **/

        private void btn_bomb_money_6_Click(object sender, EventArgs e)
        {
            if (!validateFields(6, 0))
            {
                setNewBombValue(
                utils.discoutnWithMoney(
                    6,
                    getTotalBomb(cbx_gasoline_type_6.SelectedIndex, 6),
                    getPriceBomb(cbx_gasoline_type_6.SelectedIndex, 6),
                    Convert.ToDouble(txtbx_cantidad_money_6.Text)),
                cbx_gasoline_type_6.SelectedIndex,
                6);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }

        private void btn_bombear_galones_6_Click(object sender, EventArgs e)
        {
            if (!validateFields(6, 1))
            {
                setNewBombValue(
                   utils.discountWithGallons(
                       6,
                       getTotalBomb(cbx_gasoline_type_6_1.SelectedIndex, 6),
                       getPriceBomb(cbx_gasoline_type_6_1.SelectedIndex, 6),
                       Convert.ToInt32(txtbx_quantity_gallons_6.Text)),
                   cbx_gasoline_type_6_1.SelectedIndex,
                   6);
            }
            else
            {
                MessageBox.Show("Complete la información solicitada.");
            }
        }











        /** Utilidades para las bombas **/



        private void setNewBombValue(double newValue, int gasolineType, int bombValue)
        {
            switch (gasolineType)
            {
                case 0:
                    if (!(bomb[bombValue - 1].Diesel_gasoline < 0)) bomb[bombValue - 1].Diesel_gasoline = bomb[bombValue - 1].Diesel_gasoline - newValue;
                    else MessageBox.Show("Ya no hay más gasolina en este tanque.");
                    break;
                case 1:
                    if (!(bomb[bombValue - 1].Regular_gasoline < 0)) bomb[bombValue-1].Regular_gasoline = bomb[bombValue-1].Regular_gasoline - newValue;                        
                    else MessageBox.Show("Ya no hay más gasolina en este tanque.");
                    break;
                case 2:
                    if (!(bomb[bombValue - 1].Special_gasoline < 0)) bomb[bombValue-1].Special_gasoline = bomb[bombValue-1].Special_gasoline - newValue;
                    else MessageBox.Show("Ya no hay más gasolina en este tanque.");
                    break;
            }
            setBombsInformation();
        }


        /** Get price of gasoline **/
        private double getPriceBomb(int gasolineType, int bombNumber)
        {
            double price = 0.0;
            switch (gasolineType)
            {
                case 0: //Diesel
                    price = bomb[bombNumber-1].Diesel_price;
                    break;
                case 1: //Regular
                    price = bomb[bombNumber-1].Regular_price;
                    break;
                case 2: //Especial
                    price = bomb[bombNumber-1].Special_price;
                    break;
            }
            return price;
        }

        /** Get total quantity of bomb **/
        private double getTotalBomb(int gasolineType, int bombNumber) 
        {
            double total = 0.0;
            switch (gasolineType)
            {
                case 0: //Diesel
                    total = bomb[bombNumber-1].Diesel_gasoline;
                    break;
                case 1: //Regular
                    total = bomb[bombNumber-1].Regular_gasoline;
                    break;
                case 2: //Especial
                    total = bomb[bombNumber-1].Special_gasoline;
                    break;
                default:
                    total = 0.0;
                    break;
            }
            return total;
        }




        /** 
         * 
         * 
         * Validaciones  campos.
         * 
         * 
         * */


        private Boolean validateFields(int nBomb, int typeBomb)    
        {
            switch (nBomb)
            {
                case 1:
                    if(typeBomb == 0)
                    {
                        if (cbx_gasoline_type_1.SelectedIndex == -1 || txtbx_cantidad_money_1.Text == "") return true;
                        else return false;
                    }
                    else
                    {
                        if (cbx_gasoline_type_1_1.SelectedIndex == -1 || txtbx_quantity_gallons_1.Text == "") return true;
                        else return false;
                    }
                case 2:
                    if (typeBomb == 0)
                    {
                        if (cbx_gasoline_type_2.SelectedIndex == -1 || txtbx_cantidad_money_2.Text == "") return true;
                        else return false;
                    }
                    else
                    {
                        if (cbx_gasoline_type_2_1.SelectedIndex == -1 || txtbx_quantity_gallons_2.Text == "") return true;
                        else return false;
                    }
                case 3:
                    if (typeBomb == 0)
                    {
                        if (cbx_gasoline_type_3.SelectedIndex == -1 || txtbx_cantidad_money_3.Text == "") return true;
                        else return false;
                    }
                    else
                    {
                        if (cbx_gasoline_type_3_1.SelectedIndex == -1 || txtbx_quantity_gallons_3.Text == "") return true;
                        else return false;
                    }
                case 4:
                    if (typeBomb == 0)
                    {
                        if (cbx_gasoline_type_4.SelectedIndex == -1 || txtbx_cantidad_money_4.Text == "") return true;
                        else return false;
                    }
                    else
                    {
                        if (cbx_gasoline_type_4_1.SelectedIndex == -1 || txtbx_quantity_gallons_4.Text == "") return true;
                        else return false;
                    }
                case 5:
                    if (typeBomb == 0)
                    {
                        if (cbx_gasoline_type_5.SelectedIndex == -1 || txtbx_cantidad_money_5.Text == "") return true;
                        else return false;
                    }
                    else
                    {
                        if (cbx_gasoline_type_5_1.SelectedIndex == -1 || txtbx_quantity_gallons_5.Text == "") return true;
                        else return false;
                    }
                case 6:
                    if (typeBomb == 0)
                    {
                        if (cbx_gasoline_type_6.SelectedIndex == -1 || txtbx_cantidad_money_6.Text == "") return true;
                        else return false;
                    }
                    else
                    {
                        if (cbx_gasoline_type_6_1.SelectedIndex == -1 || txtbx_quantity_gallons_6.Text == "") return true;
                        else return false;
                    }
                default:
                    break;
            }

            return false;
        }


    }
}