﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POOLaboratorioII_LR132112
{
    public partial class FrmSplashScreen : MaterialSkin.Controls.MaterialForm
    {

        private int _tricks;

        public FrmSplashScreen()
        {
            InitializeComponent();
            new classes._Util().setMaterialSkin(this);
            timerSplashScreen.Start();
        }

        private void timerSplashScreen_Tick(object sender, EventArgs e)
        {
            _tricks++;
            if(_tricks == 4)
            {
                timerSplashScreen.Stop();
                new FrmMainView().Show();
                this.Hide(); 

            }
        }
    }
}
